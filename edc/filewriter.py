import collections
import csv
import os

mongo_fields = ['_updated', '_created', '_id']


def process_items(items, resource, item_schema, writers, files, parent_id=False, first=False):

    for index, item in enumerate(items):
        field_removal_list = []
        if '_links' in item:
            del item['_links']
        if '_is_archived' in item:
            # ignore EDA field not present in the EDC schema.
            del item['_is_archived']
        for field, value in item.items():
            if field in mongo_fields:
                continue
            if item_schema[field]['type'] == 'list':

                if '_id' in item:
                    item_id = item['_id']
                elif 'origin_id' in item:
                    item_id = item['origin_id']
                elif 'parent_id' in item:
                    item_id = item['parent_id']
                else:
                    item_id = False

                field_removal_list.append(field)
                if 'schema' in item_schema[field]['schema']:
                    process_items(value, field, item_schema[field]['schema']['schema'], writers, files, item_id)
                else:

                    value_dict = [{field: string_value, 'parent_id': item_id} for string_value in value]

                    process_items(value_dict, field, {field: {'type': 'string'}, 'parent_id': {'type': 'string'}},
                                  writers, files, item_id)

        if parent_id:
            item['parent_id'] = parent_id

        for field in field_removal_list:
            del item[field]

        try:
            writers[resource].writerow(flatten(item))
            files[resource].flush()
        except ValueError as e:
            print(resource, e)
            ValueError(e)


def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, bool):
            v = int(v)
            items.append((new_key, v))
        elif isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def get_fields(dictionary):
    fields = []
    for field_name, field_data in dictionary.items():
        if field_data['type'] == 'dict':
            fields.extend([field_name + '_' + field for field in get_fields(field_data['schema'])])
        elif field_data['type'] != 'list':
            fields.append(field_name)
    return fields


def create_writers(resource, schema, location, output_path='', first=True):
    files = {}
    writers = {}

    file_name = os.path.join(output_path, location + '_' + resource + '.csv')

    files[resource] = open(file_name, 'w', encoding='utf-8')
    fieldnames = get_fields(schema)
    if first:
        fieldnames.extend(mongo_fields)
    else:
        fieldnames.append('parent_id')
    writers[resource] = csv.DictWriter(files[resource],
                                       fieldnames=fieldnames)
    writers[resource].writeheader()

    for field_name, field_data in schema.items():
        if field_data['type'] == 'list':
            subfiles = None
            subwriters = None
            if field_data['schema']['type'] == 'dict':
                subfiles, subwriters = create_writers(field_name, field_data['schema']['schema'],
                                                      location + '_' + resource, output_path, first=False)
            elif field_data['schema']['type'] == 'string':
                # This is a one-field subtable. Try it that way
                subfiles, subwriters = create_writers(field_name, {field_name: {'type': 'string'}},
                                                      location + '_' + resource, output_path, first=False)
            if subfiles:
                files.update(subfiles)
            if subwriters:
                writers.update(subwriters)

    return files, writers
