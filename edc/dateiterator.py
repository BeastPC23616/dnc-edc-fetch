import datetime
import logging

logger = logging.getLogger('edc_fetch.edc.dateiterator.DateIterator')
sort_fields = {'transactions': 'date_posted',
               'attendance': 'date',
               'glsum': 'date',
               'labor': 'apply_date',
               'itemsum': 'date',
               'reservations': 'transaction_time_stamp',
               'gamingjackpot': 'timestamp',
               'gamingoffer': 'start_date'}


class DateIterator:
    def __init__(self, apiclient):
        logger.debug('__init__' + ', '.join(map(str, [apiclient])) + ')')
        self.apiclient = apiclient
        self.location = None
        self.resource = None
        self.parameters = None
        self.first_date = None
        self.last_date = None
        self.current_date = None

    def initialize_auth(self, client_id=None, client_secret=None, api_key=None):
        return self.apiclient.initialize_auth(client_id=client_id,
                                              client_secret=client_secret,
                                              api_key=api_key)

    def get_iterator(self, location, resource, first_date, last_date=None, parameters=None):
        logger.debug('get_iterator(' + ', '.join(map(str, [location, resource, first_date, last_date, parameters])) + ')')  # noqa
        self.location = location
        self.resource = resource
        self.parameters = parameters
        self.first_date = self.current_date = first_date
        self.last_date = last_date

        return self

    def __next__(self):
        logger.debug('__next__()')
        return self.next()

    def next(self):
        logger.debug('next()')
        while self.current_date <= self.last_date:
            self.parameters[sort_fields[self.resource]] = str(self.current_date)
            may_have_more_data = True
            page = 1
            while may_have_more_data:
                logger.debug('Requesting page ' + str(page) + ' for date ' + str(self.current_date))
                response = self.apiclient.get(self.location,
                                              self.resource,
                                              page=page,
                                              parameters=self.parameters,
                                              allow_count=True)
                if response is None:
                    logger.warning('Request failed, retrying.')
                    continue
                else:
                    logger.debug('Got ' + str(len(response['_items'])) + ' records')
                may_have_more_data = len(response['_items']) == self.apiclient.max_results
                page += 1
                yield response
            self.current_date += datetime.timedelta(days=1)

        return
