import logging

logger = logging.getLogger('edc_fetch.edc.pageiterator.PageIterator')


class PageIterator:
    def __init__(self, apiclient):
        logger.debug('__init__' + ', '.join(map(str, [apiclient])) + ')')
        self.apiclient = apiclient
        self.location = None
        self.resource = None
        self.parameters = None
        self.page = None
        self.last_page = None

    def initialize_auth(self, client_id=None, client_secret=None, api_key=None):
        return self.apiclient.initialize_auth(client_id=client_id,
                                              client_secret=client_secret,
                                              api_key=api_key)

    def get_iterator(self, location, resource, parameters=None, first_page=1, last_page=None):
        logger.debug('get_iterator(' + ', '.join(map(str, [location, resource, parameters, first_page, last_page])) + ')')  # noqa
        """
        Return an interator to loop through EDC results.
        :param location:
        :param resource:
        :param parameters:
        """
        self.location = location
        self.resource = resource
        self.parameters = parameters
        self.page = first_page
        self.last_page = last_page

        return self

    def __next__(self):
        logger.debug('__next__()')
        return self.next()

    def next(self):
        logger.debug('next()')
        """
        Fetch the next batch of records from the EDC.
        :return: Dict encoded EDC response.
        """
        may_have_more_data = True
        while may_have_more_data:
            logger.debug('Requesting page ' + str(self.page))
            response = self.apiclient.get(self.location, self.resource, page=self.page, parameters=self.parameters)
            if response is None:
                logger.warning('Request failed, retrying.')
                continue
            may_have_more_data = len(response['_items']) == self.apiclient.max_results
            self.page += 1
            yield response
            if self.last_page and self.page > self.last_page:
                return

        return
