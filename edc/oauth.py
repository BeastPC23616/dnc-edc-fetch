import logging
import requests

logger = logging.getLogger('edc_fetch.oauth')


class OAuth:
    """
    OAuth2.0 authorization helper
    """
    AUTH_ENDPOINT_PROD = 'https://api.delawarenorth.com/oauth/client_credential/accesstoken?grant_type=client_credentials'  # noqa
    AUTH_ENDPOINT_TEST = 'https://dnc-test.apigee.net/oauth/client_credential/accesstoken?grant_type=client_credentials'

    def __init__(self, is_test=True, client_id=None, client_secret=None, api_key=None):
        logger.debug('OAuth.__init__(' + ', '.join(map(str, [is_test, client_id, client_secret, api_key])) + ')')
        self.endpoint = OAuth.AUTH_ENDPOINT_TEST if is_test else OAuth.AUTH_ENDPOINT_PROD
        if api_key:
            self.api_key = api_key
            self.credentials = None
            self.access_token = None
        elif client_id and client_secret:
            self.credentials = {'client_id': client_id,
                                'client_secret': client_secret}
            self.api_key = None
            self.access_token = None

    def refresh_access_token(self):
        logger.debug('OAuth.refresh_access_token()')
        if self.endpoint and self.credentials:
            try:
                response = requests.post(self.endpoint, self.credentials, None)
                self.access_token = response.json()['access_token']
            except:
                self.access_token = None
                raise RuntimeError('Invalid OAuth credentials.')
        else:
            raise RuntimeError('OAuth not ready to request access token.')

    def decorate(self, headers=None, querystring=None):
        logger.debug('OAuth.decorate(' + ', '.join(map(str, [headers, querystring])) + ')')
        if headers and self.uses_access_token():
            if self.access_token is None:
                self.refresh_access_token()
            headers['Authorization '] = 'Bearer {0}'.format(self.access_token)
        elif querystring and self.uses_api_key():
            querystring['apikey'] = self.api_key
        else:
            raise RuntimeWarning('Invalid state. Request elements not decorated by OAuth.')

    def uses_access_token(self):
        logger.debug('OAuth.uses_access_token()')  # noqa
        return self.credentials is not None

    def uses_api_key(self):
        logger.debug('OAuth.uses_api_key()')  # noqa
        return self.api_key is not None
