import datetime
import logging
import pytz

logger = logging.getLogger('edc_fetch.edc.lastmodifiediterator.LastModifiedIterator')
logger.level = logging.DEBUG

date_format = '%Y-%m-%dT%H:%M:%SZ'


class LastModifiedIterator:

    def __init__(self, apiclient):
        logger.debug('__init__' + ', '.join(map(str, [apiclient])) + ')')
        self.apiclient = apiclient
        self.total_records = 0
        self.range_description = ''
        self.max_expected_records = None
        self.location = None
        self.resource = None
        self.parameters = None
        self.last_date = None
        self.first_date = None
        self.next_date = None
        self.current_date = None

    def initialize_auth(self, client_id=None, client_secret=None, api_key=None):
        return self.apiclient.initialize_auth(client_id=client_id,
                                              client_secret=client_secret,
                                              api_key=api_key)

    def get_iterator(self, location, resource, first_date, last_date=None, parameters=None, max_expected_records=None):
        logger.debug('get_iterator(' + ', '.join(map(str, [location, resource, first_date, last_date, parameters])) + ')')  # noqa
        self.location = location
        self.resource = resource
        self.parameters = parameters
        self.first_date = self.next_date = self.current_date = first_date
        self.next_date += datetime.timedelta(hours=1)
        self.last_date = last_date if last_date else datetime.datetime.now().astimezone(pytz.utc)
        self.max_expected_records = max_expected_records if max_expected_records else None

        return self

    def __next__(self):
        logger.debug('__next__()')
        return self.next()

    def next(self):
        logger.debug('next()')
        while self.current_date < self.last_date:

            self.parameters['where'] = '{{"_updated":{{"$gt":"{0}","$lte":"{1}"}}}}'.format(
                datetime.datetime.strftime(self.current_date, date_format),
                datetime.datetime.strftime(self.next_date, date_format)
            )
            
            has_more_data = True
            page = 1
            total_records_for_this_set = 0
            while has_more_data:
                logger.debug('Request {} page {}.'.format(
                    self.current_date,
                    page
                ))
                response = self.apiclient.get(self.location,
                                              self.resource,
                                              page=page,
                                              parameters=self.parameters,
                                              allow_count=True)
                if response is None:
                    logger.warning('Request failed, retrying.')
                    continue
                else:
                    record_count = len(response['_items'])
                    total_records_for_this_set += record_count
                    self.total_records += record_count
                    logger.debug('Received {} records for {} page {} '.format(
                        record_count,
                        self.current_date,
                        page
                    ))

                page += 1
                if response and '_meta' in response and 'total' in response['_meta']:
                    has_more_data = total_records_for_this_set != response['_meta']['total']
                else:
                    has_more_data = False
                yield response

            if self.max_expected_records and self.total_records == self.max_expected_records:
                self.next_date = self.last_date

            self.current_date = self.next_date
            self.next_date += datetime.timedelta(hours=1)

            if self.next_date > self.last_date:
                self.next_date = self.last_date

            self.range_description = '{}..{}'.format(
                datetime.datetime.strftime(self.current_date, date_format),
                datetime.datetime.strftime(self.next_date, date_format),
            )

        return
