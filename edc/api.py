from .oauth import OAuth
import logging
import requests


logger = logging.getLogger('edc_fetch.edc.api.APIClient')


class EDCException(Exception):
    def __init__(self, request):
        self.request = request

    def __str__(self):
        try:
            return self.request.json()['_error']['message']
        except:
            return self.request.text + "\n" + self.request.url


class EDCTimeout(EDCException):
    pass


class APIClient:
    """
    APIClient

    A light wrapper around the Enterprise Data Cache API
    """

    BASEPATH_TEST = 'https://dnc-test.apigee.net/dnc/edc/v1'
    BASEPATH_PROD = 'https://api.delawarenorth.com/dnc/edc/v1'

    def __init__(self, use_test_environment=False, max_results=1000, timeout=30):
        logger.debug('__init__' + ', '.join(map(str, [use_test_environment, max_results])) + ')')

        self.location, self.resource, self.parameters, self.iterator_type = None, None, None, None
        self.max_results = max_results
        self.timeout = timeout

        self.basepath = self.BASEPATH_TEST if use_test_environment else self.BASEPATH_PROD  # noqa
        self.headers = {'User-Agent': 'edcfetch/2.0.0',
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'}

        self.use_test_environment = use_test_environment
        self.oauth = None

    def initialize_auth(self, client_id=None, client_secret=None, api_key=None):
        self.oauth = OAuth(self.use_test_environment,
                           client_id=client_id,
                           client_secret=client_secret,
                           api_key=api_key)
        return self.oauth

    def get(self, location, resource, page=1, parameters=None, retry_count=0, allow_count=False):
        logger.debug('get(' + ', '.join(map(str, [location, resource, page, parameters, retry_count])) + ')')  # noqa
        """
        Make a GET request to the EDC.
        :param parameters: A dictionary of query parameters.
        :param location: The business unit from which to request data.
        :param resource: The resource type requested
        :param page: The page of data requested
        :return: A python dictionary returned from the EDC.
        """

        payload = {'max_results': self.max_results,
                   'page': page}
        if not allow_count:
            payload['no_count'] = True

        if parameters:
            payload.update(parameters)

        path = '{basepath}/{location}/{resource}'.format(basepath=self.basepath,
                                                         location=location,
                                                         resource=resource)

        try:
            # decorate the headers or payload with oauth access_token or api key.
            self.oauth.decorate(headers=self.headers, querystring=payload)

            logger.debug('get():requests.get(' + str(path) + ', ' + str(payload) + ', ' + str(self.headers) + ')')
            response = requests.get(path, params=payload, headers=self.headers, timeout=self.timeout)

            # If this failed then the access token may have expired. Refresh the access token and try again ONCE.
            if response.status_code == 401 and self.oauth.uses_access_token():
                self.oauth.refresh_access_token()
                self.oauth.decorate(headers=self.headers)
                response = requests.get(path, params=payload, headers=self.headers)

            if response.status_code == 200:
                return response.json()
            elif 400 <= response.status_code <= 500:
                raise EDCException(response)
            elif response.status_code == 504:
                raise EDCTimeout(response)
            else:
                return None

        except RuntimeError as rte:
            print(rte)
            exit()
        except KeyboardInterrupt:
            exit()
        except requests.exceptions.ConnectionError:
            return None
        except EDCTimeout:
            raise
        except:
            if retry_count > 2:
                raise
            return self.get(location, resource, page, parameters, retry_count + 1, allow_count)
