# edc_fetch.py

The `edc_fetch.py` tool fetches data from the [Enterprise Data Cache][edc] for all locations and resources.
The data are downloaded in pages, and stored in a CSV format. This script works for all current resources.

If a particular resource uses nested data structures, sub-tables will be generated with a foreign key back
to the original record. For example, querying for transactions from Aaron's Aerospace Academy (location 'aaa') will generate six 
files:

- `aaa_transactions.csv`
- `aaa_transactions_price_modifiers.csv`
- `aaa_transactions_sale_items.csv`
- `aaa_transactions_sale_items_categories.csv`
- `aaa_transactions_sale_items_modifiers.csv`
- `aaa_transactions_sale_items_modifiers_categories.csv`
- `aaa_transactions_tax_items.csv`
- `aaa_transactions_tender_items.csv`

## Setup

This tool requires Python 3.4+ to be installed. For computers running
Windows, you can download python from 
[python.org/downloads](https://www.python.org/downloads/release/python-352/).
When installing, be sure to select the "Add Python to your PATH 
environment variable."

### Windows Installation
In a terminal, navigate to the location of the EDC Fetch script
```
$ dir /path/to/edc/fetch/script/
```

Now, navigate run the install.bat script.
```
$ install.bat
```

You should now be ready to use the EDC fetch script.

### *nix Installation

In a terminal, navigate to the location of the EDC Fetch script
```
$ cd /path/to/edc/fetch/script/
```

To keep the python requirements for this script contained, we will create
a virtual environment. First, let's install virtualenv, our virtual
environment handler.
```
$ pip install virtualenv
```

Now, let's create a virtualenv. We will be storing the virtual environment
in the folder `venv`, and we want to make sure that the environment is
using python 3.
```
$ virtualenv venv --python=python3
```

With the virtual environment created, let's activate it. On linux, run
```
$ source venv/bin/activate
```

or on Windows run
```
$ .\venv\Scripts\activate.bat
```

We can now install the requirements for the EDC Fetch script.
```
$ pip install -r requirements.txt
```

You should now be ready to use the EDC fetch script.

## Usage
```bash
Usage: edc_fetch_v2.py <resource> <location> [--api-key KEY | --oauth-id ID --oauth-secret SECRET]
            [--first-page FIRST ] [ --last-page LAST | --all] [--max-results RESULTS] [--filter FILTER]
            [--source SOURCE] [--last-modified DATETIME ] [--last-modified-file] [--test] [--debug]
            [--timeout SECONDS]

Options:
    -h --help               Show this screen.
    --api-key KEY           The EDC API key to use.
    --oauth-id ID           Optional OAuth client ID. Requires --oauth-secret
    --oauth-secret SECRET   Optional OAuth secret. Requires --oauth-id.
    --first-page FIRST      The first page to fetch
    --last-page LAST        The last page to fetch
    --all                   Fetch all pages
    --max-results RESULTS   the number of results to return per page.
    --filter FILTER         The filter string to apply to query with.
    --source SOURCE         Limit the records to a specific source system.
    --last-modified DATE    Date from which to return records.
    --last-modified-file    Flag indicating to use the date stored in the last modified file as the lower bound.
    --test                  Fetch data from the test environment
    --debug                 Print debugging information.
    --timeout SECONDS       User defined timeout parameter. Default is 30 seconds.

If last modified options are used the date must be in the following format expressed in UTC:

YYYY-MM-DDThh:mm:ssZ

Example: 2017-06-21T10:30:34Z
```

## Examples

### Fetch all labor records from a specific business unit.
In this case, let's fetch all labor records for Aaron's Aerospace Academy (location 'aaa').

```
$ python edc_fetch_v2.py labor aaa --all --oauth-id ID --oauth-secret SECRET

  0% (  10 of 3040) |=                                            | Elapsed Time: 0:00:20 ETA:  10:55:24
```

### Fetch all transactions from the last day from a specific business unit.
In this case, let's fetch labor records for Aaron's Aerospace Academy (location 'aaa') made on 2016-04-09.

```
$ python edc_fetch_v2.py labor aaa --filter "apply_date=2016-04-09" --all --oauth-id ID --oauth-secret SECRET

100% (1 of 1) |###################################################################################################| Elapsed Time: 0:00:00 Time: 0:00:00
```

This generates the aaa_labor.csv file with 30 labor records from 2016-04-09. All 
of the [filters listed][filters] for each resource can be used with this tool.

[filters]: https://confluence.delawarenorth.com/display/SM/EDC+REST+API+-+Labor
[edc]: https://confluence.delawarenorth.com/display/SM/Enterprise+Data+Cache+REST+API