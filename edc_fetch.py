"""EDC Fetch

Usage: edc_fetch.py <resource> <location> [--api-key KEY | --oauth-id ID --oauth-secret SECRET]
            [--first-page FIRST ] [ --last-page LAST | --all] [--max-results RESULTS] [--filter FILTER]
            [--last-id ORIGIN_ID] [--last-id-file] [--test]

Options:
    -h --help               Show this screen.
    --api-key KEY           The EDC API key to use.
    --first-page FIRST      The first page to fetch
    --last-page LAST        The last page to fetch
    --all                   Fetch all pages
    --max-results RESULTS   the number of results to return per page.
    --filter FILTER         The filter string to apply to query with.
    --test                  Fetch data from the test environment
    --oauth-id ID           Optional OAuth client ID. Requires --oauth-secret
    --oauth-secret SECRET   Optional OAuth secret. Requires --oauth-id.
    --last-id ORIGIN_ID     Optional. Used to return records greater than the last origin id.
    --last-id-file          Optional flag. Causes script to read/write last origin id.
"""

import collections
import csv
import datetime
import math
import os

import progressbar
import requests
import requests.exceptions
import schema
from docopt import docopt

last_id_filename = 'lastid.ini'

mongo_fields = ['_updated', '_created', "_id"]
sort_fields = {'transactions': "date_closed",
               'attendance': 'date',
               'glsum': 'date',
               'labor': 'apply_date',
               'itemsum': 'date',
               'reservations': 'transaction_time_stamp',
               'gamingjackpot': 'timestamp',
               'gamingoffer': 'start_date'}


class OAuth:
    """
    OAuth2.0 authorization helper
    """
    APIGEE_PROD = 'https://api.delawarenorth.com/oauth/client_credential/accesstoken?grant_type=client_credentials'
    APIGEE_TEST = 'https://dnc-test.apigee.net/oauth/client_credential/accesstoken?grant_type=client_credentials'

    def __init__(self, is_test=True, client_id=None, client_secret=None, api_key=None):
        self.endpoint = None
        self.api_key = None
        self.access_token = None
        self.credentials = None
        self.initialize(is_test, client_id, client_secret, api_key)

    def initialize(self, is_test, client_id=None, client_secret=None, api_key=None):
        self.endpoint = OAuth.APIGEE_TEST if is_test else OAuth.APIGEE_PROD
        if api_key:
            self.api_key = api_key
            self.credentials = None
            self.access_token = None
        elif client_id and client_secret:
            self.credentials = {
                'client_id': client_id,
                'client_secret': client_secret
            }
            self.api_key = None
            self.access_token = None

    def refresh_access_token(self):
        if self.endpoint and self.credentials:
            try:
                response = requests.post(self.endpoint, self.credentials, None)
                self.access_token = response.json()['access_token']
            except:
                self.access_token = None
                raise RuntimeError('Invalid OAuth credentials.')
        else:
            raise RuntimeError('OAuth not ready to request access token.')

    def decorate(self, headers=None, querystring=None):
        if headers and self.uses_access_token():
            if self.access_token is None:
                self.refresh_access_token()
            headers['Authorization '] = 'Bearer {0}'.format(self.access_token)
        elif querystring and self.uses_api_key():
            querystring['apikey'] = self.api_key
        else:
            raise RuntimeWarning('Invalid state. Request elements not decorated by OAuth.')

    def uses_access_token(self):
        return self.credentials is not None

    def uses_api_key(self):
        return self.api_key is not None


class EDCException(Exception):
    def __init__(self, request):
        self.request = request

    def __str__(self):
        return self.request.text + "\n" + self.request.url


class EDC:
    """
    EDC

    A python client for the Enterprise Data Cache.
    """

    def __init__(self, apikey, test=False, max_results=1000):
        self.location, self.resource, self.parameters, self.iterator_type = None, None, None, None
        self.apikey = apikey
        self.max_results = max_results

        self.basepath = "https://dnc-test.apigee.net/dnc/edc/v1" if test else "https://api.delawarenorth.com/dnc/edc/v1"

        self.headers = {'User-Agent': 'edcfetch/0.0.1',
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'}

        self.page = 1

        self.min_date = self.max_date = self.date = None
        self.first_page = 1
        self.last_page = None
        self.step = 0
        self.steps = 0
        self.oauth = OAuth()

    def get(self, location, resource, page=1, parameters=None, retry_count=0):
        """
        Make a GET request to the EDC.
        :param parameters: A dictionary of query parameters.
        :param location: The business unit from which to request data.
        :param resource: The resource type requested
        :param page: The page of data requested
        :param retry_count: How many times to retry
        :return: A python dictionary returned from the EDC.
        """

        payload = {'max_results': self.max_results,
                   'page': page}

        if parameters:
            payload.update(parameters)

        path = self.basepath + "/" + location + "/" + resource

        try:
            # decorate the headers or payload with oauth access_token or api key.
            self.oauth.decorate(headers=self.headers, querystring=payload)

            response = requests.get(path, params=payload, headers=self.headers)

            # If this failed then the access token may have expired. Refresh the access token and try again ONCE.
            if response.status_code == 401 and self.oauth.uses_access_token():
                self.oauth.refresh_access_token()
                self.oauth.decorate(headers=self.headers)
                response = requests.get(path, params=payload, headers=self.headers)

            if response.status_code == 200:
                return response.json()
            if 400 <= response.status_code < 500:
                raise EDCException(response)
            else:
                print(response.text)
                return None

        except RuntimeError as rte:
            print(rte)
            exit()
        except KeyboardInterrupt:
            exit()
        except requests.exceptions.ConnectionError:
            return None
        except:
            if retry_count > 2:
                raise
            return self.get(location, resource, page, parameters, retry_count + 1)

    def max_pages(self):
        """
        Return the maximum number of pages for a query.
        """
        data = self.get(self.location, self.resource, parameters=self.parameters)
        return math.ceil(data["_meta"]["total"] / data["_meta"]["max_results"])

    def get_date_range(self):
        """
        Get the minimum and maximum date for a query.
        :return:
        """
        date_field = sort_fields[self.resource]
        parameters = self.parameters.copy()

        # Get the smallest date
        parameters.update({'sort': '[("' + date_field + '",1)]', 'max_results': 1})
        response = self.get(self.location, self.resource, parameters=parameters)

        if not response:
            return self.get_date_range()

        if response["_meta"]["total"] == 0:
            exit("No results found. "+response["_message"])
        min_date = datetime.datetime.strptime(response["_items"][0][date_field], '%Y-%m-%dT%H:%M:%SZ')

        # Get the largest date
        parameters['sort'] = '[("' + date_field + '",-1)]'
        response = self.get(self.location, self.resource, parameters=parameters)

        if not response:
            return self.get_date_range()

        max_date = datetime.datetime.strptime(response["_items"][0][date_field], '%Y-%m-%dT%H:%M:%SZ')

        return min_date, max_date

    def get_iterator(self, location, resource, first_page=1, last_page=None, parameters=None):
        """
        Return an interator to loop through EDC results.
        :param last_page:
        :param first_page:
        :param parameters:
        :param resource:
        :param location:
        """
        self.location = location
        self.resource = resource
        self.parameters = parameters
        self.first_page = first_page

        # Determine if the query can be iterated by date. If it can, then to it that way so the query can use a mongo
        # index. Otherwise, page through everything.
        if resource in sort_fields and last_page is None:
            # Iterate by date
            self.iterator_type = 'date'
            self.min_date, self.max_date = self.get_date_range()
            self.date = self.min_date
            self.steps = self.get(location, resource, parameters=parameters)["_meta"]["total"]

        else:
            # Iterate by page
            self.last_page = last_page if last_page else self.max_pages()
            self.steps = (1 + last_page - first_page) * self.max_results
            self.page = self.first_page

        return self

    def __next__(self):
        return self.next()

    def next(self):
        """
        Fetch the next batch of records from the EDC.
        :return: Dict encoded EDC response.
        """
        if self.iterator_type == 'date':
            while self.date <= self.max_date:
                self.parameters[sort_fields[self.resource]] = self.date.date()
                response = self.get(self.location, self.resource, page=self.page, parameters=self.parameters)
                if response is None:
                    print("\nRequest failed. Retrying.")
                    continue

                yield response

                if self.page * self.max_results >= response['_meta']['total']:
                    self.page = 1
                    self.date += datetime.timedelta(days=1)
                else:
                    self.page += 1

            else:
                raise StopIteration()
        else:
            while self.page <= self.last_page:
                response = self.get(self.location, self.resource, page=self.page, parameters=self.parameters)
                if response is None:
                    print("\nRequest failed. Retrying.")
                    continue

                self.page += 1
                yield response
            raise StopIteration()


def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, bool):
            v = int(v)
            items.append((new_key, v))
        elif isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def parse_filter_string(filter_string):
    parameters = {}
    if filter_string != "":
        filters = filter_string.split("&")
        for in_filter in filters:
            field, value = in_filter.split("=")
            parameters[field] = value
    return parameters


def write_last_id_file(last_id):
    file = open(last_id_filename, 'w', encoding='utf-8')
    file.write(str(last_id))
    file.close()


def read_last_id_file():
    file = open(last_id_filename, 'r', encoding='utf-8')
    line = file.readline()
    last_id = 0
    if len(line) > 0:
        last_id = int(line)
    file.close()
    return last_id


def last_id_file_exists():
    return os.path.isfile(last_id_filename)


def main(arguments):
    # Setup CLI arguments
    location = arguments['<location>']
    resource = arguments['<resource>']

    if arguments['--filter']:
        filter_string = arguments['--filter']
    else:
        filter_string = ''

    # determine if the test or production environment should be used.
    use_test_environment = arguments['--test']

    # get the api key from the command line or environment if it exists.
    if arguments["--api-key"]:
        api_key = arguments["--api-key"]
    else:
        api_key = os.environ.get("EDC_API_KEY")

    # get the client id and secret from the command line or environment if they exist.
    if arguments['--oauth-id'] and arguments['--oauth-secret']:
        client_id = arguments['--oauth-id']
        client_secret = arguments['--oauth-secret']
    else:
        client_id = os.environ.get("EDC_CLIENT_ID")
        client_secret = os.environ.get("EDC_CLIENT_SECRET")

    # Create the EDC client
    edc = EDC(api_key, arguments['--test'],
              max_results=int(arguments["--max-results"]) if arguments["--max-results"] else 1000)

    # configure OAuth to use the api key or client credentials.
    if api_key:
        edc.oauth.initialize(use_test_environment, api_key=api_key)
    elif client_id and client_secret:
        edc.oauth.initialize(use_test_environment, client_id=client_id, client_secret=client_secret)
    else:
        raise Exception("Invalid authentication configuration.")

    page = int(arguments['--first-page']) if arguments['--first-page'] else 1
    max_page = int(arguments['--last-page']) if arguments['--last-page'] \
        else page + 1

    parameters = parse_filter_string(filter_string)

    # if specified, get last origin ID from CLI or saved file. Add filter to limit response to records
    # greater than the last origin ID.
    if arguments['--last-id'] or arguments['--last-id-file']:
        if arguments['--last-id']:
            origin_id = int(arguments['--last-id'])
        elif last_id_file_exists():
            origin_id = read_last_id_file()
        else:
            origin_id = None
        if origin_id:
            parameters['where'] = '{{"origin_id_int":{{"$gt":{0}}}}}'.format(origin_id)
            parameters['sort'] = '[{origin_id_int:1}]'

    if arguments['--all']:
        max_page = None

    files, writers = create_writers(resource, schema.DOMAIN[resource]['schema'], location)

    edc_iterator = edc.get_iterator(location, resource, first_page=page, last_page=max_page, parameters=parameters)
    bar = progressbar.ProgressBar(max_value=edc_iterator.steps)

    # Iterate through the pages.
    max_origin_id = None
    for data in edc_iterator.next():
        edc_iterator.step += len(data["_items"])
        bar.update(edc_iterator.step)
        if len(data["_items"]) == 0:
            continue

        max_origin_id = process_items(data['_items'],
                                      resource,
                                      schema.DOMAIN[resource]['schema'],
                                      writers,
                                      files,
                                      first=True,
                                      max_origin_id=max_origin_id)
    bar.finish()

    for _, file in files.items():
        file.close()

    if arguments['--last-id-file'] and max_origin_id:
        write_last_id_file(max_origin_id)


def process_items(items, resource, item_schema, writers, files, parent_id=False, first=False, max_origin_id=None):

    for index, item in enumerate(items):
        field_removal_list = []
        if "_links" in item:
            del item["_links"]
        if "_is_archived" in item:
            # ignore EDA field not present in the EDC schema.
            del item["_is_archived"]
        for field, value in item.items():
            if field in mongo_fields:
                continue
            if item_schema[field]["type"] == "list":

                # capture the max origin_id if an integer version exists
                if 'origin_id_int' in item and (max_origin_id is None or item['origin_id_int'] > max_origin_id):
                    max_origin_id = item['origin_id_int']

                if "_id" in item:
                    item_id = item["_id"]
                elif "origin_id" in item:
                    item_id = item["origin_id"]
                elif "parent_id" in item:
                    item_id = item["parent_id"]
                else:
                    item_id = False

                field_removal_list.append(field)
                if "schema" in item_schema[field]["schema"]:
                    process_items(value, field, item_schema[field]["schema"]['schema'], writers, files, item_id)
                else:

                    value_dict = [{field: string_value, "parent_id": item_id} for string_value in value]

                    process_items(value_dict, field, {field: {"type": "string"}, "parent_id": {"type": "string"}},
                                  writers, files, item_id)

        if parent_id:
            item["parent_id"] = parent_id

        for field in field_removal_list:
            del item[field]

        try:
            writers[resource].writerow(flatten(item))
            files[resource].flush()
        except ValueError as e:
            print(resource, e)
            ValueError(e)

    return max_origin_id


def get_fields(dictionary):
    fields = []
    for field_name, field_data in dictionary.items():
        if field_data["type"] == "dict":
            fields.extend([field_name + "_" + field for field in get_fields(field_data["schema"])])
        elif field_data["type"] != "list":
            fields.append(field_name)
    return fields


def create_writers(resource, res_schema, file_name, first=True):
    files = {}
    writers = {}

    files[resource] = open(file_name + "_" + resource + ".csv", "w", encoding='utf-8')
    fieldnames = get_fields(res_schema)
    if first:
        fieldnames.extend(mongo_fields)
    else:
        fieldnames.append("parent_id")
    writers[resource] = csv.DictWriter(files[resource],
                                       fieldnames=fieldnames)
    writers[resource].writeheader()

    for field_name, field_data in res_schema.items():
        if field_data["type"] == "list":
            subfiles = None
            subwriters = None
            if field_data["schema"]["type"] == 'dict':
                subfiles, subwriters = create_writers(field_name, field_data["schema"]["schema"],
                                                      file_name + "_" + resource, first=False)
            elif field_data["schema"]["type"] == 'string':
                # This is a one-field subtable. Try it that way
                subfiles, subwriters = create_writers(field_name, {field_name: {"type": "string"}},
                                                      file_name + "_" + resource, first=False)
            if subfiles:
                files.update(subfiles)
            if subwriters:
                writers.update(subwriters)

    return files, writers


if __name__ == "__main__":
    main(docopt(__doc__, version="EDC Fetch 0.1.6"))
