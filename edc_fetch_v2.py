"""Enterprise Data Cache Fetch Tool v2

Downloads data from the Enterprise Data Cache API as flattened CSV files

Usage: edc_fetch_v2.py <resource> <location> [--api-key KEY | --oauth-id ID --oauth-secret SECRET]
            [--first-page FIRST ] [ --last-page LAST | --all] [--max-results RESULTS] [--filter FILTER]
            [--source SOURCE] [--last-modified DATETIME ] [--last-modified-file] [--output OUTPUT_PATH]
            [--test] [--debug] [--timeout SECONDS]

Options:
    -h --help               Show this screen.
    --api-key KEY           The EDC API key to use.
    --oauth-id ID           Optional OAuth client ID. Requires --oauth-secret
    --oauth-secret SECRET   Optional OAuth secret. Requires --oauth-id.
    --first-page FIRST      The first page to fetch
    --last-page LAST        The last page to fetch
    --all                   Fetch all pages
    --max-results RESULTS   the number of results to return per page.
    --filter FILTER         The filter string to apply to query with.
    --source SOURCE         Limit the records to a specific source system.
    --last-modified DATE    Date from which to return records.
    --last-modified-file    Flag indicating to use the date stored in the last modified file as the lower bound.
    --output OUTPUT_PATH    Path to write the output files to.
    --test                  Fetch data from the test environment.
    --debug                 Print debugging information.
    --timeout SECONDS       User defined timeout parameter. Default is 30 seconds.

If last modified options are used the date must be in the following format expressed in UTC:

YYYY-MM-DDThh:mm:ssZ

Example: 2017-06-21T10:30:34Z

"""

from docopt import docopt
from edc.api import APIClient, EDCException, EDCTimeout
from edc.dateiterator import DateIterator
from edc.lastmodifiediterator import LastModifiedIterator
from edc.pageiterator import PageIterator
from urllib.parse import parse_qs
import datetime
import edc.filewriter
import iso8601
import logging
import math
import progressbar
import pytz
import os
import schema

logger = logging.getLogger('edc_fetch')

sort_fields = {'transactions': 'date_posted',
               'attendance': 'date',
               'glsum': 'date',
               'labor': 'apply_date',
               'itemsum': 'date',
               'reservations': 'transaction_time_stamp',
               'gamingjackpot': 'timestamp',
               'gamingoffer': 'start_date',
               'menuitems': 'date',
               'flights': 'scheduled_departure',
               'weather': 'datetime',
               'events': 'start_time'}


def write_last_modified_file(last_modified_filename, last_modified):
    file = open(last_modified_filename, 'w', encoding='utf-8')
    file.write(datetime.datetime.strftime(last_modified, '%Y-%m-%dT%H:%M:%SZ'))
    file.close()


def read_last_modified_file(last_modified_filename):
    file = open(last_modified_filename, 'r', encoding='utf-8')
    line = file.readline()
    last_modified = datetime.datetime.utcnow()
    if len(line) > 0:
        last_modified = iso8601.parse_date(line).astimezone(pytz.utc)
    file.close()
    return last_modified


def last_modified_file_exists(last_modified_filename):
    return os.path.isfile(last_modified_filename)


def main(arguments):
    logger.debug('main(' + str(arguments) + ')')

    execution_start = datetime.datetime.now().astimezone(pytz.utc)

    location = arguments['<location>']
    resource = arguments['<resource>']
    filter_string = arguments['--filter'] if arguments['--filter'] else ''
    use_test_environment = arguments['--test']
    api_key = arguments['--api-key'] if arguments['--api-key'] else os.environ.get('EDC_API_KEY')
    client_id = arguments['--oauth-id'] if arguments['--oauth-id'] else os.environ.get('EDC_CLIENT_ID')
    client_secret = arguments['--oauth-secret'] if arguments['--oauth-secret'] else os.environ.get('EDC_CLIENT_SECRET')
    max_results = int(arguments['--max-results']) if arguments['--max-results'] else 1000
    first_page = int(arguments['--first-page']) if arguments['--first-page'] else None
    last_page = int(arguments['--last-page']) if arguments['--last-page'] else None
    source_system = arguments['--source'] if arguments['--source'] else None
    output_path = arguments['--output'] if arguments['--output'] else None

    if resource not in schema.DOMAIN:
        print('Unknown resource "' + resource + '", unable to fetch data.')
        exit(1)

    # Light-weight API client to EDC which we use for trying to determine how many records will be downloaded and then
    # for iterating over those records for download
    apiclient = APIClient(use_test_environment=use_test_environment,
                          max_results=max_results)

    if arguments['--timeout']:
        try:
            apiclient.timeout = int(arguments['--timeout'])
            print('Timeout set to {} seconds.'.format(arguments['--timeout']))
        except Exception as e:
            print('Unable to set timeout value of {}. Using default of {} seconds.'.format(
                arguments['--timeout'],
                apiclient.timeout)
            )

    if api_key:
        apiclient.initialize_auth(api_key=api_key)
    elif client_id and client_secret:
        apiclient.initialize_auth(client_id=client_id, client_secret=client_secret)
    else:
        raise Exception('Invalid authentication configuration.')

    filter_parameters = parse_qs(filter_string)

    # if specified, get last modified date from the CLI or saved file. Add filter to limit response to records
    # greater than the last modified date.

    uses_last_modified = arguments['--last-modified'] is not None or arguments['--last-modified-file']
    last_modified_file = None
    last_modified = None

    if uses_last_modified:
        if arguments['--last-modified']:
            last_modified = iso8601.parse_date(arguments['--last-modified'])

        if arguments['--last-modified-file']:
            last_modified_file = 'lastmodified-{}-{}.ini'.format(source_system if source_system else 'all', resource)
            if last_modified_file_exists(last_modified_file) and not last_modified:
                last_modified = read_last_modified_file(last_modified_file)
            else:
                last_modified = last_modified if last_modified else datetime.datetime.now()
                write_last_modified_file(last_modified_file, last_modified)

            filter_parameters['where'] = '{{"_updated":{{"$gt":"{0}","$lte":"{1}"}}}}'.format(
                datetime.datetime.strftime(last_modified, '%Y-%m-%dT%H:%M:%SZ'),
                datetime.datetime.strftime(execution_start, '%Y-%m-%dT%H:%M:%SZ')
            )

    # Limit by source system if specified.
    if source_system:
        filter_parameters['source_system'] = source_system

    # Try to get a total record count if it can be done without a huge performance hit
    total_record_count = None
    expected_iteration_steps = None

    try:
        parameters = {'max_results': 1,
                      'projection': '{"_id":1}'}
        parameters.update(filter_parameters)
        response = apiclient.get(location=location, resource=resource, allow_count=True, parameters=parameters)
        total_record_count = int(response['_meta']['total'])
        expected_iteration_steps = int(math.ceil(total_record_count / max_results))
    except EDCTimeout:
        print('UNABLE TO DETERMINE TOTAL OF NUMBER OF RECORDS.')
        print('The progress bar displayed will not be able to provide an estimated time of completion.')
        print()
    except EDCException as e:
        print('Unable to determine total number of records because of an unexpected API failure: ' + str(e))
        exit(1)
    except Exception as e:
        print('Unexpected failure: ' + str(e))
        exit(1)

    if total_record_count == 0:
        if filter_string:
            print('No records found for {resource} records at {location} with filter "{filter}"'.format(
                resource=resource, location=location, filter=filter_string))
        else:
            print('No records found for {resource} records at {location}'.format(
                resource=resource, location=location))
        exit(1)

    if arguments['--all']:
        # Iterate by date
        if resource in sort_fields:
            date_field = sort_fields[resource]
            date_lookup_parameters = filter_parameters.copy()

            date_lookup_parameters.update({'sort': '[("' + date_field + '",1)]', 'max_results': 1})
            response = apiclient.get(location, resource, parameters=date_lookup_parameters)
            if len(response['_items']) == 0:
                print('FAILED while retrieving lower-bound date, no records found')
                exit(1)
            min_date = datetime.datetime.strptime(response['_items'][0][date_field], '%Y-%m-%dT%H:%M:%SZ').date()

            date_lookup_parameters['sort'] = '[("' + date_field + '",-1)]'
            response = apiclient.get(location, resource, parameters=date_lookup_parameters)
            if len(response['_items']) == 0:
                print('FAILED while retrieving upper-bound date, no records found')
                exit(1)
            max_date = datetime.datetime.strptime(response['_items'][0][date_field], '%Y-%m-%dT%H:%M:%SZ').date()
            expected_iteration_steps = (max_date - min_date).days + 1

            iterator_object = DateIterator(apiclient=apiclient)
            iterator = iterator_object.get_iterator(location=location,
                                                    resource=resource,
                                                    parameters=filter_parameters,
                                                    first_date=min_date,
                                                    last_date=max_date)
        # Iterate all records by page
        else:
            iterator_object = PageIterator(apiclient=apiclient)
            iterator = iterator_object.get_iterator(location=location,
                                                    resource=resource,
                                                    parameters=filter_parameters,
                                                    first_page=1)
    elif uses_last_modified:

        expected_iteration_steps = None
        iterator_object = LastModifiedIterator(apiclient=apiclient)
        iterator = iterator_object.get_iterator(location=location,
                                                resource=resource,
                                                parameters=filter_parameters,
                                                first_date=last_modified,
                                                last_date=execution_start,
                                                max_expected_records=total_record_count)

    # Iterate all records by page with a first/last page limiter
    else:
        first_page = first_page if first_page else 1
        last_page = last_page if last_page else None

        if expected_iteration_steps and first_page and first_page > expected_iteration_steps:
            print('--first-page option cannot be greater than the expected number of pages (' + str(expected_iteration_steps) + ')')  # noqa
            exit(1)
        if expected_iteration_steps and last_page and last_page > expected_iteration_steps:
            print('--last-page option cannot be greater than the expected number of pages (' + str(expected_iteration_steps) + ')')  # noqa
            exit(1)

        if expected_iteration_steps and last_page:
            expected_iteration_steps = last_page - first_page + 1
        elif expected_iteration_steps:
            expected_iteration_steps -= first_page - 1

        iterator_object = PageIterator(apiclient=apiclient)
        iterator = iterator_object.get_iterator(location=location,
                                                resource=resource,
                                                parameters=filter_parameters,
                                                first_page=first_page,
                                                last_page=last_page)

    # if output_path doesn't exist, create it
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    files, writers = edc.filewriter.create_writers(resource, schema.DOMAIN[resource]['schema'], location, output_path)

    iteration_step = 0
    if uses_last_modified:
        max_progressbar_value = total_record_count
    else:
        max_progressbar_value = expected_iteration_steps if expected_iteration_steps else progressbar.UnknownLength
    format_custom_text_widget = progressbar.FormatCustomText('%(status)s', {'status': 'Downloading...'})
    bar = progressbar.ProgressBar(max_value=max_progressbar_value,
                                  widgets=[format_custom_text_widget,
                                           ' ',
                                           progressbar.Percentage(),
                                           progressbar.Bar(marker='█', fill='░'),

                                           progressbar.ETA()])

    for data in iterator.next():
        iteration_step += 1
        if isinstance(bar.max_value, int) and iteration_step <= bar.max_value:
            if type(iterator_object) is DateIterator:
                format_custom_text_widget.update_mapping(status='Downloading ' + str(iterator.current_date))
                bar.update((iterator.current_date - iterator.first_date).days + 1)
            elif type(iterator_object) is LastModifiedIterator:
                format_custom_text_widget.update_mapping(status=iterator.range_description)
                bar.update(iterator.total_records if iterator.total_records < bar.max_value else bar.max_value)
            else:
                format_custom_text_widget.update_mapping(status='Downloading page ' + str(iteration_step))
                bar.update(iteration_step)
        if len(data['_items']) == 0:
            continue

        edc.filewriter.process_items(
            data['_items'],
            resource,
            schema.DOMAIN[resource]['schema'],
            writers,
            files,
            first=True
        )

        # Persist the current datetime in the event of a failure so processing can pick up where it left off.
        if uses_last_modified and last_modified_file:
            write_last_modified_file(last_modified_file, iterator.current_date)

    bar.finish()

    print('\nWrote files:')
    for _, file in files.items():
        print(file.name)
        file.close()

    # Write the final datetime to the last modified file.
    if uses_last_modified and last_modified_file:
        write_last_modified_file(last_modified_file, iterator.current_date)


if __name__ == '__main__':
    cli_args = docopt(__doc__, version='EDC Fetch 2.0.0')
    if cli_args['--debug']:
        logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('  >> %(levelname)s - %(asctime)s - %(name)s - %(message)s')
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        logger.addHandler(ch)
    main(cli_args)
