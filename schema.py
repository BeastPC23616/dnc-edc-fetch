"""
    Configuration settings for Enterprise Data Cache eve application

    These are the settings that are not environment specific
"""

from helpers import all_source_systems, source_systems_for
from datetime import date

PAGINATION_LIMIT = 1000
DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
JSON_SORT_KEYS = True  # JSON payload will return keys in alphabetical order

# VM-1146 - Make queries timeout before application does so queries dont keep
# running... and running... and running....
MONGO_SOCKET_TIMEOUT_MS = 25000  # 25 seconds

# VM-1165: Disable global filtering. Each collection will whitelist fields they
# can be filtered on
ALLOWED_FILTERS = []

DOMAIN = {
    'activity': {
        'allowed_filters': [
            '_id',
            '_is_archived',
            'activity',
            'actor',
            'boundary.end_time',
            'correlation_id',
            'level',
            'location._id',
            'location.subsidiary',
            'object',
            'resource',
            'source_system',
            'target',
            'time',
            'vendor_origin_id',
            'verb'
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record last update.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_creator': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record creation.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Record has been archived to EDA.',
                    'sample': True,
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            'activity': {
                'type': 'string',
                'required': True,
                'empty': False,
                'doc': {
                    'description': 'The activity being performed by the actor.',
                    'sample': 'loading',
                }
            },
            'actor': {
                'type': 'string',
                'required': True,
                'empty': False,
                'doc': {
                    'description': 'The system which is taking the action.',
                    'sample': 'MicrosTransactions',
                }
            },
            'boundary': {
                'type': 'dict',
                'required': False,
                'empty': False,
                'doc': {
                    'description': 'The time bounds of the data being acted on',
                    'sample': '{"start_time":"2016-11-11T12:51:21Z","end_time":"2016-11-11T12:51:21Z"}',
                },
                'schema': {
                    'start_time': {
                        'type': 'datetime',
                        'required': True,
                        'empty': False,
                        'doc': {
                            'description': 'The starting time boundary (inclusive) of the data being acted on.',
                            'sample': '2016-11-11T12:51:21Z',
                        }
                    },
                    'end_time': {
                        'type': 'datetime',
                        'required': True,
                        'empty': False,
                        'doc': {
                            'description': 'The ending time boundary (inclusive) of the data being acted on.',
                            'sample': '2016-11-11T12:51:21Z',
                        }
                    },
                },
            },
            'correlation_id': {
                'type': 'string',
                'required': False,
                'doc': {
                    'description': 'Optional ID of message or record from source system or actor. Should be used to '
                                   'tie various related activity records into a single chain of events. IDs do not '
                                   'follow any specific format and in fact will look wildly different depending on '
                                   'the actor and source system',
                    'sample': '1729045951',
                }
            },
            'level': {
                'type': 'string',
                'required': True,
                'empty': False,
                'allowed': ['DEBUG', 'ERROR', 'INFO', 'WARNING'],
                'doc': {
                    'description': 'See Log4J log levels for definitions.',
                    'sample': 'INFO',
                }
            },
            'location': {
                'required': False,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'required': True,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Identifier for location activity acted on.',
                            'sample': 'ksc',
                        }
                    },
                    'subsidiary': {
                        'type': 'string',
                        'required': True,
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Subsidiary code for location activity acted on.',
                            'sample': 'PARKS',
                        }
                    },
                }
            },
            'message': {
                'type': 'string',
                'required': False,
                'doc': {
                    'description': 'Generated value based on other attributes, cannot be set through the API.',
                    'sample': ('DN Validator Framework finished Nightly Load Validation of transactions from Micros to'
                               ' EDC at 2012-02-08T01:02:00Z.'),
                }
            },
            'object': {
                'type': 'string',
                'required': True,
                'empty': False,
                'doc': {
                    'description': 'The resource being acted upon.',
                    'sample': 'transactions',
                }
            },
            'object_count': {
                'type': 'integer',
                'required': False,
                'doc': {
                    'description': '(Optional) The number of objects in event',
                    'sample': 123,
                }
            },
            'source_system': {
                'type': 'string',
                'allowed': all_source_systems(),
                'required': True,
                'empty': False,
                'doc': {
                    'description': 'The source from which the object of the action originates.',
                    'sample': 'micros',
                }
            },
            'target': {
                'type': 'string',
                'required': True,
                'empty': False,
                'allowed': ['EDC', 'EDA', 'S3', 'EXTERNAL'],
                'doc': {
                    'description': 'The destination system or entity being acted against. External is any target /'
                                   'outside the DN Data stack. The actor will be used to identify specific records.',
                    'sample': 'EDC',
                }
            },
            'time': {
                'type': 'datetime',
                'required': True,
                'empty': False,
                'doc': {
                    'description': ('The time at which the activity is taking place. (May be different from when the '
                                    ' record was created)'),
                    'sample': '2016-11-11T12:51:21Z',
                }
            },
            'meaningful_date': {
                'type': 'datetime',
                'required': False,
                'empty': False,
                'doc': {
                    'description': 'The date of the data in which the process is acting upon',
                    'sample': '2016-11-11T12:51:21Z',
                }
            },
            'verb': {
                'type': 'string',
                'required': True,
                'empty': False,
                'allowed': ['finished', 'started', 'failed'],
                'doc': {
                    'description': 'The state of the action taking place',
                    'sample': 'started',
                }
            },
            'vendor_origin_id': {
                'type': 'string',
                'required': False,
                'empty': False,
                'doc': {
                    'description': "The specific venue_id for an appetize location.",
                    'sample': '1086',
                }
            },
        },
    },
    'attendance': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'cost_center.origin_id',
            'cost_center.id',
            'cost_center.name',
            'date',
            'location._id',
            'source_system',
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
            },
            'source_system': {
                'allowed': source_systems_for('attendance'),
                'required': True,
                'type': 'string',
            },
            'extract_time': {
                'required': True,
                'type': 'datetime'
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'required': True,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                    'name': {
                        'type': 'string'
                    },
                    'subsidiary': {
                        'type': 'string',
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                }
            },
            'date': {
                'required': True,
                'type': 'datetime',
            },
            'cost_center': {
                'required': True,
                'type': 'dict',
                'schema': {
                    'id': {
                        'type': 'string'
                    },
                    'name': {
                        'type': 'string'
                    },
                    'origin_id': {
                        'type': 'string'
                    },
                    'origin_name': {
                        'type': 'string'
                    },
                },
            },
            'total': {
                'required': True,
                'type': 'integer',
            },
        },
    },
    'catalog': {
        'allowed_filters': [
            '_id',
            '_updated',
            'date',
            'resource',
            'location._id',
            'location.subsidiary',
            'source_system',
            'storage_system'
        ],
        'schema': {
            'count': {
                'type': 'integer',
                'required': True,
            },
            'date': {
                'type': 'string',
                'required': True,
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'required': True,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                    'subsidiary': {
                        'type': 'string',
                        'required': True,
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                }
            },
            'resource': {
                'type': 'string',
                'required': True,
                'allowed': ['attendance', 'gamingplayer', 'gamingjackpot', 'gamingoffer', 'glsum',
                            'itemsum', 'labor', 'menuitems ', 'reservations', 'transactions', 'weather'],
            },
            'source_system': {
                'type': 'string',
                'allowed': all_source_systems(),
                'required': True,
            },
            'storage_system': {
                'type': 'string',
                'required': True,
                'allowed': ['eda', 'edc'],
            },
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
        }
    },
    'events': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'location._id',
            'source_system',
            'origin_location_id',
            'origin_id',
            'start_time',
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
            },
            'source_system': {
                'allowed': source_systems_for('events'),
                'required': True,
                'type': 'string',
            },
            'type': {
                'allowed': [
                    'UNDEFINED',
                ],
                'required': True,
                'type': 'string',
            },
            'location': {
                'required': False,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'required': True,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                    'name': {
                        'type': 'string'
                    },
                    'subsidiary': {
                        'type': 'string',
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                }
            },
            'origin_id': {
                'required': True,
                'type': 'string',
            },
            'origin_location_id': {
                'required': True,
                'type': 'string',
            },
            'name': {
                'required': True,
                'type': 'string',
            },
            'description': {
                'required': False,
                'type': 'string',
            },
            'start_time': {
                'required': True,
                'type': 'datetime',
            },
            'end_time': {
                'required': False,
                'type': 'datetime',
            },
            'access_start': {
                'required': False,
                'type': 'datetime',
            },
        },
    },
    'flights': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'location._id',
            'origin_id',
            'scheduled_departure',
            'source_system',
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
            },
            'source_system': {
                'allowed': source_systems_for('flights'),
                'required': True,
                'type': 'string',
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Identifier for location transaction belongs to',
                            'sample': 'ksc',
                            'source_systems': '*',
                        }
                    },
                    'name': {
                        'type': 'string',
                        'doc': {
                            'description': 'Name of location transaction belongs to',
                            'sample': 'Kennedy Space Center',
                            'source_systems': '*',
                        }
                    },
                    'subsidiary': {
                        'type': 'string',
                        'minlength': 1,
                        'maxlength': 8,
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Subsidiary of Delaware North which location is part of',
                            'sample': 'PARKS',
                            'source_systems': '*',
                        }
                    },
                }
            },
            'origin_id': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Synthetic, compound origin ID based off key information in the record.',
                    'sample': '20170317EWRBWIUA4143',
                    'source_systems': ['oag'],
                }
            },
            'type': {
                'required': True,
                'type': 'string',
                'allowed': ['ARRIVAL', 'DEPARTURE'],
                'doc': {
                    'description': 'Arriving or departing flight segment.',
                    'sample': 'ARRIVAL',
                    'source_systems': ['oag'],
                }
            },
            'year': {
                'required': True,
                'type': 'integer',
                'doc': {
                    'description': 'Year the flight is scheduled.',
                    'sample': 2017,
                    'source_systems': ['oag'],
                }
            },
            'month': {
                'required': True,
                'allowed': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                'type': 'integer',
                'doc': {
                    'description': 'Month the flight is scheduled.',
                    'sample': 1,
                    'source_systems': ['oag'],
                }
            },
            'airline': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'required': True,
                        'type': 'string',
                        'doc': {
                            'description': '2-character IATA airline code.',
                            'sample': 'aa',
                            'source_systems': ['oag'],
                        }
                    },
                    'name': {
                        'required': True,
                        'type': 'string',
                        'doc': {
                            'description': 'Airline name.',
                            'sample': 'American Airlines Inc.',
                            'source_systems': ['oag'],
                        }
                    },
                },
            },
            'flight_number': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': '',
                    'sample': '',
                    'source_systems': ['oag'],
                }
            },
            'departing_airport': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'required': True,
                        'type': 'string',
                        'doc': {
                            'description': '3-character IATA airport code.',
                            'sample': 'aus',
                            'source_systems': ['oag'],
                        }
                    },
                    'name': {
                        'required': True,
                        'type': 'string',
                        'doc': {
                            'description': 'Airport name.',
                            'sample': 'Austin-Bergstrom Intl',
                            'source_systems': ['oag'],
                        }
                    },
                },
            },
            'arriving_airport': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'required': True,
                        'type': 'string',
                        'doc': {
                            'description': '3-character IATA airport code.',
                            'sample': 'aus',
                            'source_systems': ['oag'],
                        }
                    },
                    'name': {
                        'required': True,
                        'type': 'string',
                        'doc': {
                            'description': 'Airport name.',
                            'sample': 'Austin-Bergstrom Intl',
                            'source_systems': ['oag'],
                        }
                    },
                },
            },
            'aircraft_code': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Code identifying the type of aircraft used for the flight.',
                    'sample': 'ERJ',
                    'source_systems': ['oag'],
                }
            },
            'aircraft_name': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Name if the aircraft type used for the flight.',
                    'sample': 'Embraer RJ 135 /140 /145',
                    'source_systems': ['oag'],
                }
            },
            'scheduled_departure': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'Scheduled departure time in UTC time.',
                    'sample': '2017-01-01T00:00:00Z',
                    'source_systems': ['oag'],
                }
            },
            'actual_departure': {
                'required': False,
                'type': 'datetime',
                'doc': {
                    'description': 'Scheduled departure time in UTC. Not present if the flight is cancelled.',
                    'sample': '2017-01-01T00:00:00Z',
                    'source_systems': ['oag'],
                }
            },
            'scheduled_arrival': {
                'required': True,
                'type': 'datetime',
                'doc': {

                    'description': 'Scheduled arrival time in UTC.',
                    'sample': '2017-01-01T00:00:00Z',
                    'source_systems': ['oag'],
                }
            },
            'actual_arrival': {
                'required': False,
                'type': 'datetime',
                'doc': {
                    'description': 'Actual arrival time in UTC. Not present if the flight is cancelled.',
                    'sample': '2017-01-01T00:00:00Z',
                    'source_systems': ['oag'],
                }
            },
            'scheduled_terminal': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Terminal the flight is scheduled to arrive.',
                    'sample': '2',
                    'source_systems': ['oag'],
                }
            },
            'actual_gate': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Gate at which the flight arrived.',
                    'sample': '',
                    'source_systems': ['oag'],
                }
            },
            'variance_in_minutes': {
                'required': False,
                'type': 'integer',
                'doc': {
                    'description': 'Variance between the scheduled and actual arrival times in minutes.',
                    'sample': '',
                    'source_systems': ['oag'],
                }
            },
            'is_cancelled': {
                'required': True,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Flag denoting whether the flight was canceled.',
                    'sample': False,
                    'source_systems': ['oag'],
                }
            },
            'is_diverted': {
                'required': True,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Flag denoting whether the flight was diverted.',
                    'sample': False,
                    'source_systems': ['oag'],
                }
            },
            'is_early': {
                'required': True,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Flag denoting whether the flight arrived early.',
                    'sample': False,
                    'source_systems': ['oag'],
                }
            },
            'is_late': {
                'required': True,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Flag denoting whether the flight arrived late.',
                    'sample': False,
                    'source_systems': ['oag'],
                }
            },
            'seats': {
                'required': True,
                'type': 'integer',
                'doc': {
                    'description': 'How many seats are on the flight.',
                    'sample': '50',
                    'source_systems': ['oag'],
                }
            },
        },
    },
    'gamingjackpot': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'location._id',
            'source_system',
            'timestamp',
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
            },
            'source_system': {
                'allowed': source_systems_for('gamingjackpot'),
                'required': True,
                'type': 'string',
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'required': True,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                    'name': {
                        'type': 'string'
                    },
                    'subsidiary': {
                        'type': 'string',
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                }
            },
            'timestamp': {
                'required': True,
                'type': 'datetime'
            },
            'is_progressive': {
                'required': True,
                'type': 'boolean'
            },
            'progressive_type_id': {
                'required': True,
                'nullable': True,
                'type': 'string'
            },
            'player_id': {
                'required': False,
                'nullable': True,
                'type': 'integer'
            },
            'cash_amount': {
                'required': True,
                'type': 'float'
            },
            'jackpot_type': {
                'required': True,
                'type': 'string'
            },
            'slot_id': {
                'required': True,
                'type': 'string'
            },
            'area': {
                'required': True,
                'type': 'string'
            },
            'section': {
                'required': True,
                'type': 'string'
            },
            'loc': {
                'required': True,
                'type': 'string'
            },
            'manufacturer_name': {
                'required': True,
                'type': 'string'
            },
            'denomination': {
                'required': True,
                'type': 'float'
            },
            'style_description': {
                'required': True,
                'type': 'string'
            },
            'cabinet_description': {
                'required': True,
                'type': 'string'
            },
            'game_description': {
                'required': False,
                'nullable': True,
                'type': 'string'
            },
            'description': {
                'required': True,
                'type': 'string'
            }
        },
    },
    'gamingoffer': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'location._id',
            'source_system',
            'promoplayerid',
            'player_id',
            'start_date',
            'end_date'
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record last update.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_creator': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record creation.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Record has been archived to EDA.',
                    'sample': True,
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            'source_system': {
                'allowed': source_systems_for('gamingoffer'),
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Reservation system transaction came from',
                    'sample': 'micros',
                    'source_systems': '*',
                }
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'required': True,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Identifier for location transaction belongs to',
                            'sample': 'ksc',
                            'source_systems': '*',
                        }
                    },
                    'name': {
                        'type': 'string',
                        'doc': {
                            'description': 'Name of location transaction belongs to',
                            'sample': 'Kennedy Space Center',
                            'source_systems': '*',
                        }
                    },
                    'subsidiary': {
                        'type': 'string',
                        'minlength': 1,
                        'maxlength': 8,
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Subsidiary of Delaware North which location is part of',
                            'sample': 'PARKS',
                            'source_systems': '*',
                        }
                    },
                }
            },
            'player_id': {
                'required': True,
                'type': 'integer',
                'doc': {
                    'description': 'WinOasis player ID',
                    'sample': '540618',
                    'source_systems': '*',
                }
            },
            'description': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Description of the player offer',
                    'sample': '1215 New Member $20FP',
                    'source_systems': '*',
                }
            },
            'amount': {
                'required': True,
                'type': 'float',
                'doc': {
                    'description': 'Amount of offer',
                    'sample': '20',
                    'source_systems': '*',
                }
            },
            'balance': {
                'required': True,
                'type': 'float',
                'doc': {
                    'description': 'Remaining offer balance',
                    'sample': '0',
                    'source_systems': '*',
                }
            },
            'start_date': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'Offer start date, UTC',
                    'sample': '2015-12-01T08:00:00Z',
                    'source_systems': '*',
                }
            },
            'end_date': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'Offer end date, UTC',
                    'sample': '2016-01-30T23:59:00Z',
                    'source_systems': '*',
                }
            },
            'promoplayerid': {
                'required': True,
                'type': 'integer',
                'doc': {
                    'description': 'Promotion player ID',
                    'sample': '23714388',
                    'source_systems': '*',
                }
            },
            'pts_balance': {
                'required': True,
                'type': 'float',
                'doc': {
                    'description': 'Player point balance',
                    'sample': '1006',
                    'source_systems': '*',
                }
            }
        },
    },
    'gamingplayer': {
        'allowed_filters': [
            '_id',
            '_updated',
            'player_id',
            'location._id',
            'source_system',
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
            },
            'source_system': {
                'allowed': source_systems_for('gamingplayer'),
                'required': True,
                'type': 'string',
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'required': True,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                    'name': {
                        'type': 'string'
                    },
                    'subsidiary': {
                        'type': 'string',
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                }
            },
            'player_id': {
                'required': True,
                'type': 'integer',
            },
            'card_number': {
                'required': True,
                'type': 'list',
                'schema': {
                    'type': 'string'
                },
            },
            'title': {
                'required': True,
                'type': 'string',
            },
            'last_name': {
                'required': True,
                'type': 'string',
            },
            'first_name': {
                'required': True,
                'type': 'string',
            },
            'middle_initial': {
                'required': True,
                'type': 'string',
            },
            'nickname': {
                'required': True,
                'type': 'string',
            },
            'printed_name': {
                'required': True,
                'type': 'string',
            },
            'gender': {
                'required': True,
                'type': 'string',
            },
            'birthdate': {
                'required': True,
                'type': 'datetime',
            },
            'email_address': {
                'required': True,
                'type': 'string',
            },
            'address_line_1': {
                'required': True,
                'type': 'string',
            },
            'address_line_2': {
                'required': True,
                'type': 'string',
            },
            'city': {
                'required': True,
                'type': 'string',
            },
            'state_province': {
                'required': True,
                'type': 'string',
            },
            'postal_code': {
                'required': True,
                'type': 'string',
            },
            'country_code': {
                'required': True,
                'type': 'string',
            },
            'phone_number': {
                'required': True,
                'type': 'string',
            },
            'rank_label': {
                'required': True,
                'type': 'string',
            },
            'cash_balance': {
                'required': True,
                'type': 'float',
            },
            'promo_balance': {
                'required': True,
                'type': 'float',
            },
            'misc_balance': {
                'required': True,
                'type': 'float',
            },
            'points_balance': {
                'required': True,
                'type': 'number',
            },
            'points_per_dollar': {
                'required': True,
                'type': 'number',
            },
        },
    },
    'glsum': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'cost_center.origin_id',
            'cost_center.id',
            'cost_center.name',
            'date',
            'location._id',
            'source_system',
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
            },
            'extract_time': {
                'required': True,
                'type': 'datetime'
            },
            'source_system': {
                'allowed': source_systems_for('glsum'),
                'required': True,
                'type': 'string'
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'required': True,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                    'name': {
                        'type': 'string'
                    },
                    'subsidiary': {
                        'type': 'string',
                        'minlength': 1,
                        'maxlength': 8,
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                }
            },
            'date': {
                'required': True,
                'type': 'datetime'
            },
            'segment1': {
                'empty': True,
                'required': True,
                'type': 'string'
            },
            'department_id': {
                'empty': True,
                'required': True,
                'type': 'string'
            },
            'gl_code': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'product_code': {
                'required': True,
                'type': 'string'
            },
            'amount': {
                'required': True,
                'type': 'float'
            },
            'debit': {
                'required': True,
                'type': 'float'
            },
            'credit': {
                'required': True,
                'type': 'float'
            },
            'center_description': {
                'empty': True,
                'required': True,
                'type': 'string'
            },
            'account_description': {
                'empty': True,
                'required': True,
                'type': 'string'
            },
            'cost_center': {
                'required': True,
                'type': 'dict',
                'schema': {
                    'name': {
                        'type': 'string'
                    },
                    'id': {
                        'type': 'string'
                    },
                    'origin_id': {
                        'type': 'string'
                    },
                    'origin_name': {
                        'type': 'string'
                    },
                },
            },
        },
    },
    'inventorydepot_closed': {
        'allowed_filters': [
            '_id',
            '_updated',
            'location._id',
            'year',
            'month',
        ],
        'schema': {
            '_id': {
                'required': True,
                'type': 'string',
            },
            '_creator': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record creation.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'required': True,
                        'type': 'string',
                        'minlength': 2,
                        'maxlength': 16,
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': False,
                        },
                    },
                    'operating_unit': {
                        'required': True,
                        'type': 'dict',
                        'schema': {
                            '_id': {
                                'required': True,
                                'type': 'string',
                            },
                        },
                    },
                },
            },
            'year': {
                'required': True,
                'type': 'integer',
                'allowed': range(date.today().year-2, date.today().year+2)
            },
            'month': {
                'required': True,
                'type': 'integer',
                'allowed': range(1, 13)
            },
            'closed': {
                'required': True,
                'type': 'boolean',
                'default': True
            },
        }
    },
    'inventorydepot_config': {
        'url': 'inventorydepot/config',
        'item_url': 'regex("[A-Za-z0-9\+]+")',
        'allowed_filters': [
            '_id',
            '_updated',
            'location._id',
        ],
        'schema': {
            '_id': {
                'required': True,
                'type': 'string',
            },
            '_creator': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record creation.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'required': True,
                        'type': 'string',
                        'minlength': 2,
                        'maxlength': 16,
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': False,
                        },
                    },
                    'operating_unit': {
                        'required': True,
                        'type': 'dict',
                        'schema': {
                            '_id': {
                                'required': True,
                                'type': 'string',
                            },
                        },
                    },
                },
            },
            'enabled': {
                'required': True,
                'type': 'boolean',
                'default': True
            },
        }
    },
    'itemsum': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'date',
            'location._id',
            'source_system',
            'type',
            'origin_location_id',
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
            },
            'location': {
                'required': False,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'required': True,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                    'name': {
                        'type': 'string'
                    },
                    'subsidiary': {
                        'type': 'string',
                        'minlength': 1,
                        'maxlength': 8,
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                }
            },
            'source_system': {
                'allowed': source_systems_for('itemsum'),
                'required': True,
                'type': 'string'
            },
            'cost_center': {
                'required': True,
                'type': 'dict',
                'schema': {
                    'id': {
                        'type': 'string'
                    },
                    'name': {
                        'type': 'string'
                    },
                    'origin_id': {
                        'type': 'string'
                    },
                    'origin_name': {
                        'type': 'string'
                    },
                },
            },
            'origin_location_id': {
                'required': False,
                'empty': False,
                'type': 'string',
            },
            'type': {
                'required': False,
                'allowed': ['sale_items', 'tax_items', 'discounts', 'tender_items'],
                'default': 'sale_items',
                'type': 'string',
            },
            'cogs': {
                'required': False,
                'type': 'float',
            },
            'date': {
                'required': True,
                'type': 'datetime',
            },
            'name': {
                'required': True,
                'type': 'string'
            },
            'plu': {
                'empty': False,
                'required': False,
                'type': 'string'
            },
            'origin_id': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'gross_amt': {
                'required': True,
                'type': 'float'
            },
            'net_amt': {
                'required': False,
                'type': 'float'
            },
            'qty': {
                'required': True,
                'type': 'integer'
            },
            'spoilage_qty': {
                'required': False,
                'type': 'integer'
            },
            'spoilage_amt': {
                'required': False,
                'type': 'float'
            },
        },
    },
    'labor': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'cost_center.origin_id',
            'cost_center.id',
            'cost_center.name',
            'location._id',
            'origin_location_id',
            'origin_id',
            'source_system',
            'apply_date',
            'adjusted_apply_date',
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
            },
            'is_deleted': {
                'required': False,
                'type': 'boolean'
            },
            'source_system': {
                'allowed': source_systems_for('labor'),
                'required': True,
                'type': 'string'
            },
            'extract_time': {
                'required': True,
                'type': 'datetime'
            },
            'origin_id': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'origin_location_id': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'location': {
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                    'name': {
                        'type': 'string'
                    },
                    'subsidiary': {
                        'type': 'string',
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                }
            },
            'cost_center': {
                'required': True,
                'type': 'dict',
                'schema': {
                    'origin_id': {
                        'type': 'string'
                    },
                    'origin_name': {
                        'type': 'string'
                    },
                    'id': {
                        'type': 'string'
                    },
                    'name': {
                        'type': 'string'
                    },
                },
            },
            'stand_code': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'apply_date': {
                'required': True,
                'type': 'datetime'
            },
            'adjusted_apply_date': {
                'required': True,
                'type': 'datetime'
            },
            'updatedtm': {
                'required': False,
                'type': 'datetime'
            },
            'job_code': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'job_name': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'job_type': {
                'empty': True,
                'required': True,
                'type': 'string'
            },
            'paycode_name': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'paycode_type': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'time_in_seconds': {
                'required': True,
                'type': 'integer'
            },
            'money_amount': {
                'required': True,
                'type': 'float'
            },
            'wage_amount': {
                'required': True,
                'type': 'float'
            },
            'startdtm': {
                'required': True,
                'type': 'datetime'
            },
            'enddtm': {
                'required': True,
                'type': 'datetime'
            },
            'emplid': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'lastnm': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'firstnm': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'middleinitialnm': {
                'required': True,
                'type': 'string',
                'minlength': 1,
                'maxlength': 1,
            },
            'laboracctname': {
                'required': True,
                'type': 'string'
            },
        },
    },
    'locations': {
        'allowed_filters': [
            '_id',
            'subsidiary',
            'time_zone',
            'currency_code',
            'resources'
        ],
        'item_url': 'regex("[A-Za-z]+")',
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_id': {
                'unique': True,
                'empty': False,
                'minlength': 2,
                'maxlength': 16,
                'required': True,
                'type': 'string',
            },
            'coordinates': {
                'empty': False,
                'required': True,
                'type': 'point',
            },
            'address': {
                'empty': True,
                'required': False,
                'type': 'string',
            },
            'name': {
                'empty': False,
                'minlength': 1,
                'maxlength': 128,
                'required': True,
                'type': 'string',
            },
            'subsidiary': {
                'empty': False,
                'minlength': 1,
                'maxlength': 8,
                'required': True,
                'type': 'string',
                'unique': False,
                'data_relation': {
                    'resource': 'subsidiaries',
                    'field': '_id',
                    'embeddable': True,
                },
            },
            'time_zone': {
                'empty': False,
                'minlength': 3,
                'maxlength': 32,
                'required': True,
                'type': 'string',
            },
            'currency_code': {
                'empty': False,
                'default': 'USD',
                'minlength': 3,
                'maxlength': 3,
                'required': True,
                'type': 'string',
            },
            'operating_units': {
                'required': False,
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'id': {
                            'required': True,
                            'type': 'string',
                        },
                        'name': {
                            'required': False,
                            'type': 'string',
                        }
                    }
                }
            },
            'resources': {
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'name': {
                            'required': True,
                            'type': 'string',
                            'empty': False
                        },
                        'source_systems': {
                            'type': 'list',
                            'schema': {
                                'type': 'dict',
                                'schema': {
                                    'name': {
                                        'type': 'string',
                                        'required': True,
                                        'empty': False
                                    },
                                    'active': {
                                        'type': 'boolean',
                                        'required': True
                                    },
                                    'vendor_origin_id': {
                                        'type': 'list',
                                        'required': False
                                    },
                                }
                            }
                        }
                    }
                }
            },
            'airport': {
                'required': False,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'required': True,
                        'type': 'string',
                        'doc': {
                            'description': '3-character IATA airport code.',
                            'sample': 'aus',
                        }
                    },
                    'name': {
                        'type': 'string',
                        'doc': {
                            'description': 'Airport name.',
                            'sample': 'Austin-Bergstrom International Airport',
                        }
                    },
                },
                'doc': {
                    'description': 'Optional. Specified if the location is an airport.',
                    'sample': 'aus',
                }
            },
            'is_tenant': {
                'required': False,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Location is a virtual location that is tied to a tenant.',
                    'sample': True
                }
            }
        },
    },
    'menuitems': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'date',
            'origin_id',
            'location._id',
            'source_system',
            'cost_center.origin_id'
        ],
        'resource_methods': [
            'GET', 'POST', 'DELETE'
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record last update.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_creator': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record creation.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Record has been archived to EDA.',
                    'sample': True,
                    'source_systems': '*',
                    'is_metadata': True,
                },
            },
            'source_system': {
                'allowed': source_systems_for('menuitems'),
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'System record came from',
                    'sample': 'halo',
                    'source_systems': '*',
                }
            },
            'location': {
                'required': False,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'required': True,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Identifier for location record belongs to',
                            'sample': 'cvl',
                            'source_systems': '*',
                        }
                    },
                    'name': {
                        'type': 'string',
                        'doc': {
                            'description': 'Name of location record belongs to',
                            'sample': 'Cleveland Indians - Progressive Field',
                            'source_systems': '*',
                        }
                    },
                    'subsidiary': {
                        'type': 'string',
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Subsidiary of Delaware North which location is part of',
                            'sample': 'SSUS',
                            'source_systems': '*',
                        }
                    },
                }
            },
            'cost_center': {
                'required': True,
                'type': 'dict',
                'schema': {
                    'id': {
                        'type': 'string',
                        'doc': {
                            'description': ('Mapped id of cost center, such as id of concession stand or airport store'
                                            '. This value is mapped per location only for some locations.'),
                            'sample': '011',
                            'source_systems': '*',
                        }
                    },
                    'name': {
                        'type': 'string',
                        'doc': {
                            'description': ('Mapped name of cost center, such as name of concession stand or '
                                            'airport store. This value is mapped per location only for '
                                            'some locations.'),
                            'sample': 'SLE F&B',
                            'source_systems': '*',
                        }
                    },
                    'origin_id': {
                        'type': 'string',
                        'doc': {
                            'description': ('Id of cost center, in source system, such as id of concession'
                                            ' stand or airport store'),
                            'sample': '7',
                            'source_systems': '*',
                        }
                    },
                    'origin_name': {
                        'type': 'string',
                        'doc': {
                            'description': ('Name of cost center, in source system, such as name of concession'
                                            ' stand or airport store'),
                            'sample': 'SLE Pavilion',
                            'source_systems': '*',
                        }
                    },
                },
            },
            'origin_id': {
                'empty': False,
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Identifier of menu item in source system',
                    'sample': '27662345235',
                    'source_systems': '*',
                }
            },
            'date': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Date which menu item and attributes were present and loaded into EDC',
                    'sample': '2017-06-01',
                    'source_systems': '*',
                }
            },
            'name': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Name of menu item',
                    'sample': 'Hot Dog',
                    'source_systems': '*',
                }
            },
            'description': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Description of menu item',
                    'sample': 'Regular sized hot dog on a plain bun',
                    'source_systems': '*',
                }
            },
            'unit_price': {
                'required': True,
                'type': 'float',
                'doc': {
                    'description': 'Price which menu item is being sold for',
                    'sample': 4.99,
                    'source_systems': '*',
                }
            },
            'categories': {
                'empty': False,
                'required': True,
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'category_id': {
                            'type': 'string',
                            'doc': {
                                'description': 'Mapped identifier of category which menu item belongs to',
                                'sample': '123',
                                'source_systems': '*',
                            }
                        },
                        'name': {
                            'type': 'string',
                            'doc': {
                                'description': 'Mapped name of category which menu item belongs to',
                                'sample': 'Hot Food',
                                'source_systems': '*',
                            }
                        },
                        'origin_id': {
                            'type': 'string',
                            'doc': {
                                'description': 'Identifier of category which menu item belongs to, from source system',
                                'sample': '456',
                                'source_systems': '*',
                            }
                        },
                        'origin_name': {
                            'type': 'string',
                            'doc': {
                                'description': 'Name of category which menu item belongs to, from source system',
                                'sample': 'DNC-FOOD-HOT',
                                'source_systems': '*',
                            }
                        }
                    }
                }
            }
        }
    },
    'reports': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'location._id',
            'location.operating_unit.id',
            'source_system',
            'type',
            'frequency',
            'start_time',
            'end_time',
            'effective_date',
            'audit.status',
            'descriptors',
            'get_presigned_url'
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
            },
            'source_system': {
                'allowed': source_systems_for('reports'),
                'required': True,
                'type': 'string',
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'required': True,
                        'unique': False,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'EDC identifier for location transaction belongs to',
                            'sample': 'ksc',
                            'source_systems': '*',
                        }
                    },
                    'subsidiary': {
                        'type': 'string',
                        'minlength': 1,
                        'maxlength': 8,
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Subsidiary of Delaware North which location is part of',
                            'sample': 'PARKS',
                            'source_systems': '*',
                        }
                    },
                    'operating_unit': {
                        'required': True,
                        'type': 'dict',
                        'schema': {
                            'id': {
                                'required': True,
                                'type': 'string',
                                'doc': {
                                    'description': 'Operating Unit id of operating unit report belongs to',
                                    'sample': '12345',
                                    'source_systems': '*',
                                }
                            },
                            'name': {
                                'required': False,
                                'type': 'string',
                                'doc': {
                                    'description': 'Operating Unit name of operating unit report belongs to',
                                    'sample': 'Kennedy Space Center',
                                    'source_systems': '*',
                                }
                            }
                        }
                    }
                }
            },
            'effective_date': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Meaningful date used in the S3 object key. Determined by the integration. '
                                   'Examples: Daily sales report would use the date the sales report is for. '
                                   'Monthly sales report would be set for the first day of the month the report '
                                   'is for. Ideally the date used is the date a person would intuitively look '
                                   'for when searching for a given report.',
                    'sample': '2016-08-01',
                    'source_systems': '*',
                }
            },
            'extract_time': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'Approximate or exact at best date and time the report was generated.',
                    'sample': '2016-08-01T01:23:00Z',
                    'source_systems': '*',
                }
            },
            'frequency': {
                'required': True,
                'type': 'string',
                'allowed': ['daily', 'monthly', 'period'],
                'doc': {
                    'description': 'Report frequency enum.',
                    'sample': 'daily',
                    'source_systems': '*',
                }
            },
            'type': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Report type enum.',
                    'sample': 'sales',
                    'source_systems': '*',
                }
            },
            'start_time': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'Report datetime range start.',
                    'sample': '2016-08-01T01:23:00Z',
                    'source_systems': '*',
                }
            },
            'end_time': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'Report datetime range end.',
                    'sample': '2016-08-01T01:23:00Z',
                    'source_systems': '*',
                }
            },
            'descriptors': {
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'name': {
                            'required': True,
                            'type': 'string',
                            'doc': {
                                'description': 'Descriptor name. ',
                                'sample': 'Revenue Center',
                                'source_systems': '*',
                            }
                        },
                        'value': {
                            'required': True,
                            'type': 'string',
                            'doc': {
                                'description': 'Value for the given descriptor.',
                                'sample': '4763',
                                'source_systems': '*',
                            }
                        },
                    }
                },
                'doc': {
                    'description': 'Arbitrary list of key-value pairs used to describe key factors which make'
                                   'the report unique.',
                    'source_systems': '*',
                }
            },
            'report': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Report file name when POSTed to the API. Saved as s3 key within schema. The report'
                                   'URI and metadata in S3 with GETs. The href returned in the metadata should be the only'
                                   'URI used to download report files from S3. The href is a presigned URL with a timeout'
                                   'defined in the the EDC config. It is currently defaulted to 5 minutes, but refer to the'
                                   'S3_URL_EXPIRY_SECONDS setting for the actual timeout. '
                                   'The "file" attribute in the metadata is generated by EVE and shouldn''t typically be '
                                   'used. The href attribute should take precedence in most cases.',
                    'sample': '"report": {'
                              '"content_type": "application/pdf", '
                              '"file": "https://s3.amazonaws.com/delawarenorth-admin-enterprise-data-cache-report-files/test/reports/ats/micros3700/2016-08-01/sample.pdf",'
                              '"href": "https://delawarenorth-admin-enterprise-data-cache-report-files.s3.amazonaws.com/test/reports/ats/micros3700/2016-08-01/sample.pdf?Signature=8KMtnEsaabwIk8IdtvFc8nOyFlI%3D&Expires=1512138964&AWSAccessKeyId=AKIAI5ZPXH2M2ZR6L24Q",'
                              '"size": 33032'
                              '}',
                    'source_systems': '*',
                }
            },
            'audit': {
                'required': False,
                'type': 'dict',
                'schema': {
                    'status': {
                        'type': 'string',
                        'required': True,
                        'allowed': ['upload_pending', 'uploaded', 'under_review', 'accepted', 'rejected', 'void'],
                        'doc': {
                            'description': 'Current audit state using statuses defined for the Inventory Depot app.',
                            'sample': 'accepted',
                            'source_systems': '*',
                        }
                    },
                    'date': {
                        'type': 'datetime',
                        'required': True,
                        'doc': {
                            'description': 'Timestamp for the state change expressed in UTC.',
                            'sample': '2016-08-01T01:23:00Z',
                            'source_systems': '*',
                        }
                    },
                    'user': {
                        'type': 'string',
                        'required': True,
                        'doc': {
                            'description': 'AD user name of the auditor.',
                            'sample': 'JSmith',
                            'source_systems': '*',
                        }
                    },
                    'comments': {
                        'type': 'string',
                        'required': True,
                        'doc': {
                            'description': 'Comments by the auditor.',
                            'sample': 'PARKS',
                            'source_systems': '*',
                        }
                    },
                    'log': {
                        'type': 'list',
                        'required': True,
                        'schema': {
                            'type': 'dict',
                            'schema': {
                                'status': {
                                    'type': 'string',
                                    'required': True,
                                    'allowed': [
                                        'upload_pending',
                                        'uploaded',
                                        'under_review',
                                        'accepted',
                                        'rejected',
                                        'void'
                                    ],
                                },
                                'date': {
                                    'type': 'datetime',
                                    'required': True,
                                },
                                'user': {
                                    'type': 'string',
                                    'required': True,
                                },
                                'comments': {
                                    'type': 'string',
                                    'required': True,
                                },
                            },
                        },
                        'doc': {
                            'description': 'List of all audit status changes for historical purposes. Schema is the same'
                                           'as the parent audit schema less the log attribute.',
                            'sample': '',
                            'source_systems': '*',
                        }
                    }
                },
                'doc': {
                    'description': 'Optional audit state and log. Required by the Inventory Depot app.',
                    'sample': '',
                    'source_systems': '*',
                },
            },
        },
    },
    'reservations': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'extract_time',
            'transaction_time_stamp',
            'source_system',
            'location._id',
            'departure_date',
            'chain_id',
        ],
        'resource_methods': [
            'GET', 'POST', 'DELETE'
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string'
            },
            '_creator': {
                'required': False,
                'type': 'string'
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
            },
            'origin_id': {
                'empty': False,
                'required': True,
                'type': 'string'
            },
            'source_system': {
                'allowed': source_systems_for('reservations'),
                'required': True,
                'type': 'string'
            },
            'extract_time': {
                'required': True,
                'type': 'datetime'
            },
            'location': {
                'required': False,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                    'name': {
                        'type': 'string'
                    },
                    'subsidiary': {
                        'type': 'string',
                        'minlength': 1,
                        'maxlength': 8,
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,
                        },
                    },
                }
            },
            'action_type': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'adr': {
                'empty': True,
                'required': False,
                'type': 'float'
            },
            'adult_count': {
                'empty': True,
                'required': False,
                'type': 'integer'
            },
            'arrival_date': {
                'empty': True,
                'required': False,
                'type': 'datetime'
            },
            'arrival_dow': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'billing_description': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'booking_lead_time': {
                'empty': True,
                'required': False,
                'type': 'integer'
            },
            'brand_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'brand_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'cancel_date': {
                'empty': True,
                'required': False,
                'type': 'datetime'
            },
            'cancel_number': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'chain_id': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'chain_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'channel': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'channel_connect_confirm_no': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'children_count': {
                'empty': True,
                'required': False,
                'type': 'integer'
            },
            'commission_percent': {
                'empty': True,
                'required': False,
                'type': 'float'
            },
            'confirm_date': {
                'empty': True,
                'required': False,
                'type': 'datetime'
            },
            'confirm_number': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'consortia_count': {
                'empty': True,
                'required': False,
                'type': 'integer'
            },
            'consortia_name_if_part_of_one_consortia_only': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'corporate_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'credit_card_type': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'cro_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'currency': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_area': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_city': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_company': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_country': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_email': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_id': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_phone_number': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_region': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_state': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_street_address1': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_street_address2': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'customer_zip_postal_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'departure_date': {
                'empty': True,
                'required': False,
                'type': 'datetime'
            },
            'departure_dow': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'fax_notification_count': {
                'empty': True,
                'required': False,
                'type': 'integer'
            },
            'guest_first_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'guest_last_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'hotel_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'hotel_id': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'hotel_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'itinerary_number': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'loyalty_level_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'loyalty_level_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'loyalty_number': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'loyalty_program': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'market_segment_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'market_source_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'membership_number': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'nights': {
                'empty': True,
                'required': False,
                'type': 'integer'
            },
            'opt_in': {
                'empty': True,
                'required': False,
                'type': 'boolean'
            },
            'original_room_type_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'original_room_type_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'pms_rate_type_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'pms_room_type_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'primary_guest': {
                'empty': True,
                'required': False,
                'type': 'boolean'
            },
            'profile_type_selection': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'promotional_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'rate_category_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'rate_category_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'rate_type_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'rate_type_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'reservation_revenue': {
                'empty': True,
                'required': False,
                'type': 'float'
            },
            'room_type_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'room_type_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'room_upsell': {
                'empty': True,
                'required': False,
                'type': 'boolean'
            },
            'room_upsell_revenue': {
                'empty': True,
                'required': False,
                'type': 'float'
            },
            'rooms': {
                'empty': True,
                'required': False,
                'type': 'integer'
            },
            'salutation': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'sap_id': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'sec_cur_room_upsell_revenue': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'secondary_currency': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'secondary_currency_adr': {
                'empty': True,
                'required': False,
                'type': 'float'
            },
            'secondary_currency_exchange_rate': {
                'empty': True,
                'required': False,
                'type': 'float'
            },
            'secondary_currency_reservation_revenue': {
                'empty': True,
                'required': False,
                'type': 'float'
            },
            'secondary_currency_total_dynamic_package_revenue': {
                'empty': True,
                'required': False,
                'type': 'float'
            },
            'secondary_source': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'share_with': {
                'empty': True,
                'required': False,
                'type': 'boolean'
            },
            'status': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'sub_source': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'sub_source_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'total_dynamic_package_revenue': {
                'empty': True,
                'required': False,
                'type': 'float'
            },
            'total_guest_count': {
                'empty': True,
                'required': False,
                'type': 'integer'
            },
            'transaction_time_stamp': {
                'empty': True,
                'required': False,
                'type': 'datetime'
            },
            'travel_agency_city': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agency_country': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agency_fax': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agency_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agency_phone': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agency_state': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agency_street_address': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agency_street_address2': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agency_zip_postal_code': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agent_area': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agent_email': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_agent_region': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'travel_industry_id': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'user_login': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'vip_level': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'xbe_shell_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
            'xbe_template_name': {
                'empty': True,
                'required': False,
                'type': 'string'
            },
        },
    },
    'subsidiaries': {
        'allowed_filters': [
            '_id',
        ],
        'item_methods': [
            'GET'
        ],
        'item_title': 'subsidiary',
        'schema': {
            '_id': {
                'required': True,
                'type': 'string',
                'default': False,
                'doc': {
                    'description': 'EDC subsidiary id. Used to map current subsidiary id to PMAX business unit',
                    'sample': 'SSUS',
                },
            },
            'name': {
                'required': True,
                'type': 'string',
                'default': False,
                'doc': {
                    'description': 'PMAX business unit name',
                    'sample': 'US Sportservice',
                },
            },
            'business_unit': {
                'required': True,
                'type': 'string',
                'default': False,
                'doc': {
                    'description': 'PMAX business unit id',
                    'sample': 'B2000',
                },
            },
        }
    },
    'test': {
        'schema': {
            'test': {'type': 'string'},
        },
    },
    'transactions': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'cost_center.origin_id',
            'cost_center.origin_name',
            'cost_center.id',
            'cost_center.name',
            'cost_center.table_number',
            'date_closed',
            'date_posted',
            'extract_time',
            'location._id',
            'origin_id',
            'origin_id_int',
            'price_modifiers.code',
            'source_system',
            'event.origin_id'
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record last update.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_creator': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record creation.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Record has been archived to EDA.',
                    'sample': True,
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            'origin_id': {
                'empty': False,
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Unique (per location) identifier for transaction which comes from the '
                                   'source system',
                    'sample': '4323923',
                    'source_systems': '*',
                }
            },
            'origin_id_int': {
                'empty': False,
                'required': False,
                'type': 'integer',
                'doc': {
                    'description': 'Integer representation of origin_id field, if numeric. For optimization purposes.',
                    'sample': 4323923,
                    'source_systems': ['quest', 'micros3700', 'infogenesis_cloud'],
                }
            },
            'source_system': {
                'allowed': source_systems_for('transactions'),
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'POS system transaction came from',
                    'sample': 'micros',
                    'source_systems': '*',
                }
            },
            'event': {
                'required': False,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'type': 'objectid',
                        'data_relation': {
                            'resource': 'events',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Unique ID for the EDC Event',
                            'sample': 'AD73C6DBA17B25F0F1BF3AC4',
                            'source_systems': [],
                        }
                    },
                    'origin_id': {
                        'required': False,
                        'type': 'string',
                        'doc': {
                            'description': 'Primary key for the event in the source system.',
                            'sample': '209',
                            'source_systems': ['appetize'],
                        }
                    },
                    'origin_name': {
                        'required': False,
                        'type': 'string',
                        'doc': {
                            'description': 'Name for the event in the source system.',
                            'sample': 'Jets vs Dolphins',
                            'source_systems': ['appetize'],
                        }
                    },
                },
                'doc': {
                    'description': 'Event the tranaction is related to.',
                    'sample': '',
                    'source_systems': [],
                }
            },
            'extract_time': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'When the transaction was extracted from the source system',
                    'sample': '2016-08-01T01:23:00Z',
                    'source_systems': '*',
                }
            },
            'pos_type': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Type of POS transaction came from, such as WALK IN or INTERNET',
                    'sample': 'WALK IN',
                    'source_systems': ['rpro', 'infogenesis_cloud'],
                }
            },
            'number': {
                'required': True,
                'type': 'integer',
                'doc': {
                    'description': ('Number printed on receipt. Not unique, usually sequential. Cycles each day or '
                                    'through out day'),
                    'sample': 120,
                    'source_systems': '*',
                }
            },
            'covers': {
                'required': False,
                'type': 'integer',
                'doc': {
                    'description': 'Number of guests served on a check',
                    'sample': '4',
                    'source_systems': ['infogenesis', 'infogenesis_cloud', 'halo', 'micros3700',
                                       'micros', 'micros_cloud'],
                }
            },
            'date_opened': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'When transaction was started',
                    'sample': '2016-07-01T12:01:00Z',
                    'source_systems': '*',
                }
            },
            'date_closed': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'When transaction was finished',
                    'sample': '2016-07-01T12:01:00Z',
                    'source_systems': '*',
                }
            },
            'date_posted': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'When transaction was posted to Back-of-House system',
                    'sample': '2016-07-01T12:01:00Z',
                    'source_systems': '*',
                }
            },
            'origin_employee_id': {
                'empty': True,
                'required': True,
                'type': 'string',
                'nullable': True,
                'doc': {
                    'description': 'Identifier for employee who processed transaction. May be a name or an employee id',
                    'sample': 'JSMITH',
                    'source_systems': '*',
                }
            },
            'origin_employee_first': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'First name of employee who processed transaction, if available.',
                    'sample': 'John',
                    'source_systems': ['halo', 'micros3700', 'netsuite', 'quest', 'infogenesis_cloud'],
                }
            },
            'origin_employee_last': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Last name of employee who processed transaction, if available.',
                    'sample': 'Smith',
                    'source_systems': ['halo', 'micros3700', 'netsuite', 'quest', 'infogenesis_cloud'],
                }
            },
            'comment': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Transaction level comments.',
                    'sample': 'test',
                    'source_systems': ['quest'],
                }
            },
            'shipping_amount': {
                'required': False,
                'type': 'float',
                'doc': {
                    'description': 'Cost of shipping for transaction.',
                    'sample': 4.99,
                    'source_systems': ['rpro'],
                }
            },
            'tax_amount': {
                'required': False,
                'type': 'float',
                'doc': {
                    'description': 'Total tax paid on transaction',
                    'sample': 2.41,
                    'source_systems': '*',
                }
            },
            'total': {
                'required': True,
                'type': 'float',
                'doc': {
                    'description': 'Final amount paid by customer for transaction, factoring in tax and discounts.',
                    'sample': 22.37,
                    'source_systems': '*',
                }
            },
            'destination': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'The destination of the order',
                    'sample': 'drive through',
                    'source_systems': ['halo'],
                }
            },
            'order_status': {
                'required': False,
                'type': 'string',
                'allowed': ['sale', 'overring', 'void', 'return', 'no_sale', 'waste', 'cancel'],
                'doc': {
                    'description': 'Final status of order.',
                    'sample': 'sale',
                    'source_systems': ['halo', 'micros3700', 'rpro', 'titbit', 'infogenesis_cloud', 'infogenesis'],
                }
            },
            'cost_center': {
                'required': True,
                'type': 'dict',
                'schema': {
                    'name': {
                        'type': 'string',
                        'doc': {
                            'description': ('Mapped name of cost center, such as name of concession '
                                            'stand or airport store. This value is mapped per location only for '
                                            'some locations.'),
                            'sample': 'SLE F&B',
                            'source_systems': '*',
                        }
                    },
                    'id': {
                        'type': 'string',
                        'doc': {
                            'description': ('Mapped id of cost center, such as id of concession stand or airport store'
                                            '. This value is mapped per location only for some locations.'),
                            'sample': '011',
                            'source_systems': '*',
                        }
                    },
                    'origin_id': {
                        'type': 'string',
                        'doc': {
                            'description': ('Id of cost center, in source system, such as id of concession'
                                            ' stand or airport store'),
                            'sample': '7',
                            'source_systems': '*',
                        }
                    },
                    'origin_name': {
                        'type': 'string',
                        'doc': {
                            'description': ('Name of cost center, in source system, such as name of concession'
                                            ' stand or airport store'),
                            'sample': 'SLE Pavilion',
                            'source_systems': '*',
                        }
                    },
                    'origin_terminal_id': {
                        'required': False,
                        'nullable': True,
                        'type': 'string',
                        'doc': {
                            'description': 'The origin id in the source system of the terminal/POS device',
                            'sample': '54673',
                            'source_systems': ['micros', 'micros3700', 'netsuite', 'quest',
                                               'infogenesis_cloud', 'infogenesis'],
                        }
                    },
                    'origin_terminal_description': {
                        'required': False,
                        'nullable': True,
                        'type': 'string',
                        'doc': {
                            'description': 'A description of the terminal/POS device',
                            'sample': 'TC-3 BAR #01',
                            'source_systems': ['micros', 'micros3700', 'quest', 'infogenesis_cloud', 'infogenesis'],
                        }
                    },
                    'table_number': {
                        'required': False,
                        'type': 'string',
                        'doc': {
                            'description': 'If at a restaurant, the table number the order will be delivered to.',
                            'sample': '10',
                            'source_systems': ['halo', 'micros3700', 'quest', 'infogenesis_cloud'],
                        }
                    }
                },
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Identifier for location transaction belongs to',
                            'sample': 'ksc',
                            'source_systems': '*',
                        }
                    },
                    'name': {
                        'type': 'string',
                        'doc': {
                            'description': 'Name of location transaction belongs to',
                            'sample': 'Kennedy Space Center',
                            'source_systems': '*',
                        }
                    },
                    'subsidiary': {
                        'type': 'string',
                        'minlength': 1,
                        'maxlength': 8,
                        'data_relation': {
                            'resource': 'subsidiaries',
                            'field': '_id',
                            'embeddable': True,

                        },
                        'doc': {
                            'description': 'Subsidiary of Delaware North which location is part of',
                            'sample': 'PARKS',
                            'source_systems': '*',
                        }
                    },
                }
            },
            'sale_items': {
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'quantity': {
                            'required': True,
                            'type': 'integer',
                            'doc': {
                                'description': 'How many of this specific item were sold as part of this transaction',
                                'sample': 1,
                                'source_systems': '*',
                            }
                        },
                        'units': {
                            'required': False,
                            'type': 'float',
                            'doc': {
                                'description': ('How many units of this specific item that were sold as part of this '
                                                'transaction.'),
                                'sample': '13.24',
                                'source_systems': ['halo', 'infogenesis_cloud']
                            }
                        },
                        'unit_of_measure': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'The unit of measure used to determine the number of units sold.',
                                'sample': 'oz',
                                'source_systems': ['halo', 'infogenesis_cloud']
                            }
                        },
                        'third_party_id': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Identifier which relates sale item to a third party system',
                                'sample': 1,
                                'source_systems': ['halo'],
                            }
                        },
                        'name': {
                            'required': True,
                            'type': 'string',
                            'doc': {
                                'description': 'Name of sale item, such as name of item on a menu',
                                'sample': 'Burger',
                                'source_systems': '*',
                            }
                        },
                        'desc': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Description of sale item',
                                'sample': 'Glass Woodbridge Estates Chardonnay',
                                'source_systems': ['quest'],
                            }
                        },
                        'vendor_code': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Identifier for product from vendor, usually only for retail.',
                                'sample': 'EASTKO',
                                'source_systems': ['rpro'],
                            }
                        },
                        'amount': {
                            'required': True,
                            'type': 'float',
                            'doc': {
                                'description': ('Extended price of the product at time of sale. '
                                                'Does not include any discounts'),
                                'sample': 5.99,
                                'source_systems': '*',
                            }
                        },
                        'cost': {
                            'required': False,
                            'type': 'float',
                            'doc': {
                                'description': 'Product of the unit cost of the sale item multiplied by the quantity.',
                                'sample': 2.00,
                                'source_systems': 'halo',
                            }
                        },
                        'origin_id': {
                            'empty': False,
                            'required': True,
                            'type': 'string',
                            'doc': {
                                'description': 'Identifier of product in source system',
                                'sample': '2766',
                                'source_systems': '*',
                            }
                        },
                        'date_posted': {
                            'required': True,
                            'type': 'datetime',
                            'doc': {
                                'description': ('When item was added to transaction, if available. If not, same as '
                                                'date_posted value on transaction.'),
                                'sample': '2016-08-01T01:12:41Z',
                                'source_systems': '*',
                            }
                        },
                        'status': {
                            'required': False,
                            'type': 'string',
                            'allowed': ['sale', 'delete', 'cancel', 'void', 'waste'],
                            'doc': {
                                'description': 'Final status of sale item on transaction',
                                'sample': 'sale',
                                'source_systems': ['halo', 'infogenesis_cloud', 'infogenesis'],
                            }
                        },
                        'origin_employee_id': {
                            'empty': True,
                            'required': False,
                            'type': 'string',
                            'nullable': True,
                            'doc': {
                                'description': 'Employee ID associated with the sale item. May be a name or an id',
                                'sample': 'JSMITH',
                                'source_systems': ['halo', 'micros3700', 'infogenesis_cloud'],
                            }
                        },
                        'origin_employee_first': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'First name of employee associated with the sale item, if available.',
                                'sample': 'John',
                                'source_systems': ['halo', 'micros3700', 'infogenesis_cloud'],
                            }
                        },
                        'origin_employee_last': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Last name of employee associated with the sale item, if available.',
                                'sample': 'Smith',
                                'source_systems': ['halo', 'micros3700', 'infogenesis_cloud'],
                            }
                        },
                        'comment': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Comment on sale item',
                                'sample': 'foobar',
                                'source_systems': ['halo'],
                            }
                        },
                        'categories': {
                            'required': False,
                            'type': 'list',
                            'schema': {
                                'type': 'dict',
                                'schema': {
                                    'name': {
                                        'type': 'string',
                                        'doc': {
                                            'description': 'Name or value within category type',
                                            'sample': '712374',
                                            'source_systems': ['halo', 'micros', 'micros3700', 'netsuite', 'quest',
                                                               'infogenesis_cloud'],
                                        }
                                    },
                                    'type': {
                                        'type': 'string',
                                        'doc': {
                                            'description': 'Type of category',
                                            'sample': 'classid',
                                            'source_systems': ['halo', 'micros', 'micros3700', 'netsuite', 'quest',
                                                               'infogenesis_cloud'],
                                        }
                                    },
                                    'origin_id': {
                                        'type': 'string',
                                        'doc': {
                                            'description': 'Unique identifier for category in source system',
                                            'sample': '26',
                                            'source_systems': ['halo', 'micros3700', 'quest', 'infogenesis_cloud'],
                                        }
                                    }
                                },
                            },
                            'doc': {
                                'description': ('Categories which a sale item sold belongs to. Types of '
                                                'categories differ per source system'),
                                'source_systems': ['micros'],
                                'sample': [
                                    {
                                        'type': 'classid',
                                        'name': '712374'
                                    },
                                    {
                                        'type': 'major',
                                        'name': 'FOOD'
                                    },
                                    {
                                        'type': 'family',
                                        'name': 'Snack/Candy'
                                    }
                                ]
                            },
                        },
                        'is_tax_exempt': {
                            'required': False,
                            'type': 'boolean',
                            'doc': {
                                'description': 'If product is exempt from sales tax, if available in source system.',
                                'sample': True,
                                'source_systems': '*',
                            }
                        },
                        'modifiers': {
                            'required': False,
                            'type': 'list',
                            'schema': {
                                'type': 'dict',
                                'schema': {
                                    'third_party_id': {
                                        'required': False,
                                        'type': 'string',
                                        'doc': {
                                            'description': 'Identifier that relates to a value in a third party system',
                                            'sample': '7592',
                                            'source_systems': ['halo'],
                                        }
                                    },
                                    'origin_id': {
                                        'required': False,
                                        'type': 'string',
                                        'doc': {
                                            'description': 'The original identifier of the modifier in the '
                                                           'source system',
                                            'sample': '9689412',
                                            'source_systems': ['halo'],
                                        }
                                    },
                                    'line_id': {
                                        'required': False,
                                        'type': 'string',
                                        'doc': {
                                            'description': 'N/A',
                                            'sample': '82516',
                                            'source_systems': ['halo'],
                                        }
                                    },
                                    'ref_id': {
                                        'required': False,
                                        'type': 'string',
                                        'doc': {
                                            'description': ('Unique back of house identifier used to identify each '
                                                            'modifier in a sale'),
                                            'sample': '82514',
                                            'source_systems': ['halo'],
                                        }
                                    },
                                    'name': {
                                        'required': False,
                                        'type': 'string',
                                        'doc': {
                                            'description': 'Name of the modifier',
                                            'sample': 'Lge Combo Egg Norm',
                                            'source_systems': ['halo'],
                                        }
                                    },
                                    'amount': {
                                        'required': True,
                                        'type': 'float',
                                        'doc': {
                                            'description': 'Amount associated with the modifier',
                                            'sample': 2.00,
                                            'source_systems': ['halo'],
                                        }
                                    },
                                    'quantity': {
                                        'required': True,
                                        'type': 'float',
                                        'doc': {
                                            'description': 'Number of modifiers (usually 1)',
                                            'sample': 1,
                                            'source_systems': ['halo'],
                                        }
                                    },
                                    'status': {
                                        'required': False,
                                        'type': 'string',
                                        'allowed': ['sale', 'delete', 'cancel', 'void'],
                                        'doc': {
                                            'description': 'Final status of a modifier',
                                            'sample': 'sale',
                                            'source_systems': ['halo'],
                                        }
                                    },
                                    'categories': {
                                        'required': False,
                                        'type': 'list',
                                        'schema': {
                                            'type': 'dict',
                                            'schema': {
                                                'name': {
                                                    'type': 'string',
                                                    'doc': {
                                                        'description': 'Name of the category in the source system',
                                                        'sample': 'DNC-Sandwhich',
                                                        'source_systems': ['halo'],
                                                    }
                                                },
                                                'type': {
                                                    'type': 'string',
                                                    'doc': {
                                                        'description': 'Type of category',
                                                        'sample': 'minor',
                                                        'source_systems': ['halo'],
                                                    }
                                                },
                                                'origin_id': {
                                                    'type': 'string',
                                                    'doc': {
                                                        'description': 'Identifier of the category in the '
                                                                       'source system',
                                                        'sample': '1364476',
                                                        'source_systems': ['halo'],
                                                    }
                                                }
                                            },
                                        },
                                        'doc': {
                                            'description': 'Categories of the modifier',
                                            'sample': [
                                                {
                                                    'type': 'major',
                                                    'name': 'DNC Food',
                                                    'origin_id': '98723'
                                                },
                                                {
                                                    'type': 'minor',
                                                    'name': 'DNCF-S Pizza',
                                                    'origin_id': '297835'
                                                }
                                            ],
                                            'source_systems': ['halo'],
                                        }
                                    },
                                },
                            },
                        },
                    },
                },
                'doc': {
                    'description': 'Products/items sold',
                    'sample': '',
                    'source_systems': '*',
                },
            },
            'tender_items': {
                'required': False,
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'type': {
                            'required': True,
                            'type': 'string',
                            'doc': {
                                'description': 'Type of payment',
                                'sample': 'Cash',
                                'source_systems': '*',
                            }
                        },
                        'authorization_id': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'If credit, a authorization identifier returned by the payment provider',
                                'sample': 'Cash',
                                'source_systems': '*',
                            }
                        },
                        'third_party_id': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Mapped identifier which relates to payment type in a '
                                               'third party system',
                                'sample': '',
                                'source_systems': ['halo', 'netsuite', 'appetize', 'micros', 'micros3700'],
                            }
                        },
                        'charge_tip': {
                            'required': False,
                            'type': 'float',
                            'doc': {
                                'description': 'Amount of tip added to payment',
                                'sample': 5.00,
                                'source_systems': ['halo', 'micros', 'micros3700', 'quest', 'infogenesis_cloud'],
                            }
                        },
                        'amount': {
                            'required': True,
                            'type': 'float',
                            'doc': {
                                'description': 'Amount charged for this payment type',
                                'sample': 12.42,
                                'source_systems': '*',
                            }
                        },
                        'date_posted': {
                            'required': True,
                            'type': 'datetime',
                            'doc': {
                                'description': ('When payment was added to transaction, if available. If not, same as '
                                                'date_posted value on transaction.'),
                                'sample': '2016-08-01T01:12:41Z',
                                'source_systems': '*',
                            }
                        },
                        'origin_employee_id': {
                            'empty': True,
                            'required': False,
                            'type': 'string',
                            'nullable': True,
                            'doc': {
                                'description': 'Employee ID associated with the payment. May be a name or an id',
                                'sample': 'JSMITH',
                                'source_systems': ['halo', 'micros3700', 'infogenesis_cloud'],
                            }
                        },
                        'origin_employee_first': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'First name of employee associated with the payment, if available.',
                                'sample': 'John',
                                'source_systems': ['halo', 'micros3700', 'infogenesis_cloud'],
                            }
                        },
                        'origin_employee_last': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Last name of employee associated with the payment, if available.',
                                'sample': 'Smith',
                                'source_systems': ['halo', 'micros3700', 'infogenesis_cloud'],
                            }
                        },
                        'payment_status': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Status of the payment, if available.',
                                'sample': 'COMPLETED',
                                'source_systems': ['appetize'],
                            }
                        },
                        'gateway_comment': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Detailed information about the status of the payment, if available.',
                                'sample': 'This transaction has been declined. Please try again later',
                                'source_systems': ['appetize'],
                            }
                        },
                        'last4': {
                            'required': False,
                            'type': 'string',
                            'maxlength': 4,
                            'doc': {
                                'description': 'Last four number of the credit card used, if available.',
                                'sample': '0168',
                                'source_systems': ['appetize'],
                            }
                        },
                        'is_card_present': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Whether a card was present, if available.',
                                'sample': '1',
                                'source_systems': ['appetize'],
                            }
                        },
                    },
                },
                'doc': {
                    'description': 'Payments on transaction',
                    'sample': '',
                    'source_systems': '*',
                },
            },
            'tax_items': {
                'required': False,
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'name': {
                            'required': True,
                            'type': 'string',
                            'doc': {
                                'description': 'Name of tax',
                                'sample': 'State Tax',
                                'source_systems': '*',
                            }
                        },
                        'sale_item_origin_id': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': ('If available, which sale item tax applies to.'
                                                ' Maps back to sale_items.origin id'),
                                'sample': '7053',
                                'source_systems': ['quest', 'micros3700', 'rpro'],
                            }
                        },
                        'amount': {
                            'required': True,
                            'type': 'float',
                            'doc': {
                                'description': 'Amount of tax charged to transaction for this tax type',
                                'sample': 0.52,
                                'source_systems': '*',
                            }
                        },
                        'date_posted': {
                            'required': True,
                            'type': 'datetime',
                            'doc': {
                                'description': ('When tax was added to transaction, if available. If not, same as '
                                                'date_posted value on transaction.'),
                                'sample': '2016-08-01T01:12:41Z',
                                'source_systems': '*',
                            }
                        },
                        'is_inclusive': {
                            'required': False,
                            'type': 'boolean',
                            'doc': {
                                'description': 'If this tax is already included as part of a sale item amount',
                                'sample': True,
                                'source_systems': ['quest', 'micros3700'],
                            }
                        },
                    },
                },
                'doc': {
                    'description': 'Taxes charged on transaction',
                    'sample': '',
                    'source_systems': '*',
                },
            },
            'price_modifiers': {
                'required': False,
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'code': {
                            'required': True,
                            'type': 'string',
                            'doc': {
                                'description': 'A code representing what type of modifier/discount this is',
                                'sample': 'discount',
                                'source_systems': '*',
                            }
                        },
                        'name': {
                            'empty': False,
                            'required': True,
                            'type': 'string',
                            'doc': {
                                'description': 'Name of price modifier/discount',
                                'sample': 'Discount',
                                'source_systems': '*',
                            }
                        },
                        'sale_item_origin_id': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Sale item modifier applies to, if available. '
                                               'Maps to sale_items.origin_id',
                                'sample': '123',
                                'source_systems': ['halo', 'micros3700', 'netsuite', 'quest', 'rpro'],
                            }
                        },
                        'sale_item_modifier_ref_id': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': ('Sale item modifier this price modifier applies to, if available. '
                                                'Maps to sale_items.modifiers.ref_id'),
                                'sample': '123',
                                'source_systems': ['halo'],
                            }
                        },
                        'third_party_id': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Identifier which relates to third party system',
                                'sample': '123',
                                'source_systems': ['halo'],
                            }
                        },
                        'status': {
                            'required': False,
                            'type': 'string',
                            'allowed': ['sale', 'delete', 'cancel', 'void'],
                            'doc': {
                                'description': 'Final status of price modifier/discount',
                                'sample': 'sale',
                                'source_systems': ['halo', 'infogenesis_cloud'],
                            }
                        },
                        'date_posted': {
                            'required': True,
                            'type': 'datetime',
                            'doc': {
                                'description': ('When price modifier or discount was added to transaction, '
                                                'if available. If not, same as date_posted value on transaction.'),
                                'sample': '2016-08-01T01:12:41Z',
                                'source_systems': '*',
                            }
                        },
                        'amount': {
                            'required': True,
                            'type': 'float',
                            'doc': {
                                'description': 'Amount of modifier or discount',
                                'sample': 1.00,
                                'source_systems': '*',
                            }
                        },
                        'origin_employee_id': {
                            'empty': True,
                            'required': False,
                            'type': 'string',
                            'nullable': True,
                            'doc': {
                                'description': 'Employee ID associated with the price modifier. May be a name or an id',
                                'sample': 'JSMITH',
                                'source_systems': ['halo', 'micros3700'],
                            }
                        },
                        'origin_employee_first': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'First name of employee associated with the price modifier, '
                                               'if available.',
                                'sample': 'John',
                                'source_systems': ['halo', 'micros3700'],
                            }
                        },
                        'origin_employee_last': {
                            'required': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Last name of employee associated with the price modifier, '
                                               'if available.',
                                'sample': 'Smith',
                                'source_systems': ['halo', 'micros3700'],
                            }
                        },
                    },
                },
                'doc': {
                    'description': 'Discounts or other items that modified the final total',
                    'sample': '',
                    'source_systems': '*',
                },
            },
            'fee_items': {
                'required': False,
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'origin_id': {
                            'required': False,
                            'nullable': True,
                            'type': 'string',
                            'doc': {
                                'description': 'Identifier for fee type in source system',
                                'sample': '123',
                                'source_systems': ['rpro'],
                            }
                        },
                        'type': {
                            'empty': True,
                            'nullable': False,
                            'required': True,
                            'type': 'string',
                            'doc': {
                                'description': 'Type of fee',
                                'sample': 'DOGTAG',
                                'source_systems': ['appetize', 'micros3700', 'rpro', 'infogenesis_cloud'],
                            }
                        },
                        'amount': {
                            'required': True,
                            'type': 'float',
                            'doc': {
                                'description': 'Amount of fee',
                                'sample': 1.00,
                                'source_systems': ['rpro', 'infogenesis_cloud'],
                            }
                        },
                    },
                },
                'doc': {
                    'description': 'Fees charged on transaction',
                    'sample': '',
                    'source_systems': '*',
                },
            },
            'customer': {
                'required': False,
                'type': 'dict',
                'doc': {
                    'description': 'RESERVED FOR FUTURE USE',
                },
                'schema': {
                    'origin_id': {
                        'required': False,
                        'type': 'string',
                        'doc': {
                            'description': 'Identifier of customer in the source system',
                            'source_systems': ['appetize'],
                            'sample': '123',
                        },
                    },
                    'name': {
                        'required': False,
                        'type': 'string',
                        'doc': {
                            'description': 'Name of customer',
                            'source_systems': ['appetize'],
                            'sample': 'Bob Smith',
                        },
                    },
                    'first_name': {
                        'required': False,
                        'type': 'string',
                        'doc': {
                            'description': 'First name of customer',
                            'source_systems': [],
                            'sample': 'Bob',
                        },
                    },
                    'last_name': {
                        'required': False,
                        'type': 'string',
                        'doc': {
                            'description': 'Last name of customer',
                            'source_systems': [],
                            'sample': 'Smith',
                        },
                    },
                    'addresses': {
                        'required': False,
                        'type': 'list',
                        'schema': {
                            'type': 'dict',
                            'schema': {
                                'type': {
                                    'required': False,
                                    'type': 'string',
                                    'allowed': ['company'],
                                    'doc': {
                                        'description': 'Type of customer address',
                                        'source_systems': ['appetize'],
                                        'sample': 'company',
                                    },
                                },
                                'address1': {
                                    'required': False,
                                    'type': 'string',
                                    'doc': {
                                        'description': 'Customer address line #1',
                                        'source_systems': ['appetize'],
                                        'sample': '123 Main St.',
                                    },
                                },
                                'address2': {
                                    'required': False,
                                    'type': 'string',
                                    'doc': {
                                        'description': 'Customer address line #2',
                                        'source_systems': ['appetize'],
                                        'sample': 'Apt 5B',
                                    },
                                },
                                'city': {
                                    'required': False,
                                    'type': 'string',
                                    'doc': {
                                        'description': 'Customer city',
                                        'source_systems': ['appetize'],
                                        'sample': 'Anytown',
                                    },
                                },
                                'state': {
                                    'required': False,
                                    'type': 'string',
                                    'doc': {
                                        'description': 'Customer state',
                                        'source_systems': ['appetize'],
                                        'sample': 'NY',
                                    },
                                },
                                'zip': {
                                    'required': False,
                                    'type': 'string',
                                    'doc': {
                                        'description': 'Customer zip code',
                                        'source_systems': ['appetize'],
                                        'sample': '12345',
                                    },
                                },
                            }
                        }
                    },
                    'phone_numbers': {
                        'required': False,
                        'type': 'list',
                        'schema': {
                            'type': 'dict',
                            'schema': {
                                'type': {
                                    'required': False,
                                    'type': 'string',
                                    'doc': {
                                        'description': 'Customer phone type',
                                        'source_systems': ['appetize'],
                                        'sample': 'mobile',
                                    },
                                },
                                'phone_number': {
                                    'required': False,
                                    'type': 'string',
                                    'doc': {
                                        'description': 'Customer phone number',
                                        'source_systems': ['appetize'],
                                        'sample': '5555551234',
                                    },
                                },
                            }
                        }
                    },
                    'email_address': {
                        'required': False,
                        'type': 'string',
                        'doc': {
                            'description': 'Customer email address',
                            'source_systems': ['appetize'],
                            'sample': 'customer@example.com',
                        },
                    },
                    'fax_number': {
                        'required': False,
                        'type': 'string',
                        'doc': {
                            'description': 'Customer fax number',
                            'source_systems': ['appetize'],
                            'sample': '5555551234',
                        },
                    },
                    'is_deleted': {
                        'required': False,
                        'type': 'boolean',
                        'default': False,
                        'doc': {
                            'description': 'Is customer deleted from the source system',
                            'source_systems': ['appetize'],
                            'sample': False
                        },
                    },
                    'member_type': {
                        'required': False,
                        'type': 'string',
                        'doc': {
                            'description': 'RESERVED FOR FUTURE USE',
                        },
                    },
                },
            },
            'loyalty': {
                'required': False,
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'type': {
                            'required': True,
                            'nullable': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Loyalty system type',
                                'sample': 'SKIDATA',
                                'source_systems': ['appetize', 'micros', 'micros3700', 'netsuite'],
                            }
                        },
                        'origin_id': {
                            'required': False,
                            'nullable': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Customer loyalty ID',
                                'sample': '3459873',
                                'source_systems': ['appetize', 'micros', 'micros3700', 'netsuite'],
                            }
                        },
                        'barcode': {
                            'required': False,
                            'nullable': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Customer loyalty barcode',
                                'sample': '1800000000014121',
                                'source_systems': ['appetize', 'micros', 'micros3700', 'netsuite'],
                            }
                        },
                    },
                },
                'doc': {
                    'description': 'Data from 3rd party loyalty systems',
                    'sample': '',
                    'source_systems': '*',
                },
            },
            'references': {
                'required': False,
                'type': 'list',
                'schema': {
                    'type': 'dict',
                    'schema': {
                        'type': {
                            'required': True,
                            'nullable': False,
                            'type': 'string',
                            'allowed': ['transfer', 'split_check', 'reopened'],
                            'doc': {
                                'description': 'Reference type.',
                                'sample': 'transfer',
                                'source_systems': ['micros_cloud'],
                            }
                        },
                        'direction': {
                            'required': True,
                            'nullable': False,
                            'type': 'string',
                            'allowed': ['to', 'from'],
                            'doc': {
                                'description': 'Direction of the reference.',
                                'sample': 'from',
                                'source_systems': ['micros_cloud'],
                            }
                        },
                        'origin_id': {
                            'required': True,
                            'nullable': False,
                            'type': 'string',
                            'doc': {
                                'description': 'Origin ID of the transaction reference.',
                                'sample': '3459873',
                                'source_systems': ['micros_cloud'],
                            }
                        },
                    },
                },
                'doc': {
                    'description': 'List of origin_id the transaction is related to along with the relationship type.',
                    'sample': '',
                    'source_systems': '*',
                },
            }
        },
    },
    'validation': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'date',
            'location._id',
            'location.subsidiary',
            'needs_validation',
            'passed',
            'resource',
            'source_system',
            'validation_storage_system1',
            'validation_storage_system2',
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record last update.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_creator': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record creation.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Record has been archived to EDA.',
                    'sample': True,
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            'date': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Meaningful date of the EDC Data Unit being validated',
                    'sample': '2017-01-01',
                    'source_systems': '*',
                }
            },
            'last_run_time': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'The last time at which validation was run for this EDC Data Unit',
                    'sample': '2017-01-01T12:23:45Z',
                    'source_systems': '*',
                },
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Identifier for location transaction belongs to',
                            'sample': 'ksc',
                            'source_systems': '*',
                        }
                    },
                }
            },
            'passed': {
                'required': True,
                'type': 'boolean',
                'doc': {
                    'description': ('Flag indicating whether the last run of validation on this EDC Data Unit was '
                                    'successful.  '),
                    'sample': True,
                    'source_systems': '*',
                }
            },
            'resource': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Type of record',
                    'sample': 'transactions',
                    'source_systems': '*',
                }
            },
            'needs_validation': {
                'required': True,
                'type': 'boolean',
                'doc': {
                    'description': ('Flag indicating validation is required for this EDC Data Unit. This can be set to '
                                    'true even if validation has already been run.'),
                    'sample': True,
                    'source_systems': '*',
                }
            },
            'source_system': {
                'allowed': all_source_systems(),
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'system records came from',
                    'sample': 'micros',
                    'source_systems': '*',
                }
            },
            'validation_storage_system1': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': ('Validation occurs between 2 storage systems (Source, EDC, or EDA). '
                                    'This is the first system.'),
                    'sample': 'source',
                    'source_systems': '*',
                }
            },
            'validation_storage_system2': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': ('Validation occurs between 2 storage systems (Source, EDC, or EDA). '
                                    'This is the second system.'),
                    'sample': 'source',
                    'source_systems': '*',
                }
            },
        },
    },
    'validation_issues': {
        'allowed_filters': [
            '_id',
            '_updated',
            '_is_archived',
            'validation_id',
            'location._id',
            'resource',
            'source_system',
            'date',
            'type'
        ],
        'resource_methods': [
            'GET', 'POST', 'DELETE'
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record last update.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_creator': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record creation.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Record has been archived to EDA.',
                    'sample': True,
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            'validation_id': {
                'required': True,
                'type': 'objectid',
                'doc': {
                    'description': 'A reference id to the validation record that generated this issue.',
                    'sample': "ObjectId(\"59a5b33f8feea7e0a9fe9ffd\")",
                    'source_systems': '*'
                }
            },
            'resource': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'The type of record validated.',
                    'sample': 'transactions',
                    'source_systems': '*',
                }
            },
            'source_system': {
                'allowed': all_source_systems(),
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'The system that validated records came from.',
                    'sample': 'micros',
                    'source_systems': '*',
                }
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Identifier for location that the validated record belongs to.',
                            'sample': 'ksc',
                            'source_systems': '*',
                        }
                    },
                }
            },
            'date': {
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Meaningful date of the EDC Data Unit that was validated.',
                    'sample': '2017-01-01',
                    'source_systems': '*',
                }
            },
            'type': {
                'type': 'string',
                'required': False,
                'doc': {
                    'description': 'Short code indicating the type of issue.',
                    'sample': 'RECORD_MISSING',
                    'source_systems': '*',
                },
            },
            'message': {
                'type': 'string',
                'required': False,
                'doc': {
                    'description': ('An optional message with additional information about the issue. '
                                    'This should not be included if the message is simply restating the '
                                    'error type.'),
                    'sample': 'The line item with ID 4 refers to another line item (3) that does not exist.',
                    'source_systems': '*',
                },
            },
            'unique_id': {
                'type': 'dict',
                'required': False,
                'nullable': True,
                'doc': {
                    'description': ('A collection of key value pairs that '
                                    'together uniquely identify a record.'),
                    'sample': '{"origin_id":"123422352351"}',
                    'source_systems': '*',
                },
            }
        }
    },
    'weather': {
        'allowed_filters': [
            '_id',
            '_is_archived',
            '_updated',
            'source_system',
            'location._id',
            'datetime',
            'type',
        ],
        'schema': {
            '_updater': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record last update.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_creator': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Source of record creation.',
                    'sample': '7e628579-adb0-45de-a976-33578092512d',
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            '_is_archived': {
                'required': False,
                'type': 'boolean',
                'default': False,
                'doc': {
                    'description': 'Record has been archived to EDA.',
                    'sample': True,
                    'source_systems': '*',
                    'is_metadata': True,
                }
            },
            'location': {
                'required': True,
                'type': 'dict',
                'schema': {
                    '_id': {
                        'unique': False,
                        'type': 'string',
                        'data_relation': {
                            'resource': 'locations',
                            'field': '_id',
                            'embeddable': True,
                        },
                        'doc': {
                            'description': 'Identifier for location transaction belongs to',
                            'sample': 'ksc',
                            'source_systems': '*',
                        }
                    },
                },
            },
            'datetime': {
                'required': True,
                'type': 'datetime',
                'doc': {
                    'description': 'Timestamp of weather date (YYYY-MM-DDThh:mm:ssZ)',
                    'sample': '2017-06-01T9:00:00Z',
                    'source_systems': '*',
                },
            },
            'source_system': {
                'allowed': ['darksky'],
                'required': True,
                'type': 'string',
                'default': 'darksky',
                'doc': {
                    'description': 'Source of weather data',
                    'sample': 'darksky',
                },
            },
            'type': {
                'allowed': ['actual', 'forecast'],
                'required': True,
                'type': 'string',
                'doc': {
                    'description': 'Record type',
                    'sample': 'forecast',
                },
            },
            'summary': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Human readable description',
                    'sample': 'Partly cloudy overnight.',
                    'source_systems': 'darksky',
                },
            },
            'icon': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Machine readable description',
                    'sample': 'partly-cloudy-night',
                    'source_systems': 'darksky',
                },
            },
            'precipitation_intensity': {
                'required': False,
                'type': 'float',
                'min': 0,
                'doc': {
                    'description': 'Precipitation intensity (in / hr)',
                    'sample': '0.0143',
                    'source_systems': 'darksky',
                },
            },
            'precipitation_probability': {
                'required': False,
                'type': 'float',
                'min': 0,
                'max': 1,
                'doc': {
                    'description': 'Probability of precipitation occurring (0-1)',
                    'sample': '0.85',
                    'source_systems': 'darksky',
                },
            },
            'precipitation_type': {
                'required': False,
                'type': 'string',
                'doc': {
                    'description': 'Type of precipitation (rain, snow, etc)',
                    'sample': 'rain',
                    'source_systems': 'darksky',
                },
            },
            'precipitation_accumulation': {
                'required': False,
                'type': 'float',
                'min': 0,
                'doc': {
                    'description': 'The amount of snowfall accumulation (in)',
                    'sample': '0.088',
                    'source_systems': 'darksky',
                },
            },
            'temperature': {
                'required': False,
                'type': 'float',
                'doc': {
                    'description': 'Temperature (degrees Fahrenheit)',
                    'sample': '65.46',
                    'source_systems': 'darksky',
                },
            },
            'apparent_temperature': {
                'required': False,
                'type': 'float',
                'doc': {
                    'description': 'Apparent (feels like) temperature (degrees Fahrenheit)',
                    'sample': '62.11',
                    'source_systems': 'darksky',
                },
            },
            'dew_point': {
                'required': False,
                'type': 'float',
                'doc': {
                    'description': 'Dew point (degrees Fahrenheit)',
                    'sample': '48.72',
                    'source_systems': 'darksky',
                },
            },
            'humidity': {
                'required': False,
                'type': 'float',
                'min': 0,
                'max': 1,
                'doc': {
                    'description': 'Relative humidity (0-1)',
                    'sample': '0.72',
                    'source_systems': 'darksky',
                },
            },
            'wind_speed': {
                'required': False,
                'type': 'float',
                'min': 0,
                'doc': {
                    'description': 'Wind speed (mph)',
                    'sample': '8.02',
                    'source_systems': 'darksky',
                },
            },
            'wind_bearing': {
                'required': False,
                'type': 'integer',
                'min': 0,
                'max': 359,
                'doc': {
                    'description': 'Direction the wind is coming from in degrees (0=N, 90=E, 180=S, 270=W)',
                    'sample': '115',
                    'source_systems': 'darksky',
                },
            },
            'wind_gust': {
                'required': False,
                'type': 'float',
                'min': 0,
                'doc': {
                    'description': 'Wind gust speed (mph)',
                    'sample': '19.16',
                    'source_systems': 'darksky',
                },
            },
            'visibility': {
                'required': False,
                'type': 'float',
                'min': 0,
                'doc': {
                    'description': 'Average visibility (mi)',
                    'sample': '4.15',
                    'source_systems': 'darksky',
                },
            },
            'cloud_cover': {
                'required': False,
                'type': 'float',
                'min': 0,
                'max': 1,
                'doc': {
                    'description': 'Percentage of the sky occluded by clouds (0-1)',
                    'sample': '0.29',
                    'source_systems': 'darksky',
                },
            },
            'pressure': {
                'required': False,
                'type': 'float',
                'min': 0,
                'doc': {
                    'description': 'Sea-level air pressure (mbar)',
                    'sample': '1014.98',
                    'source_systems': 'darksky',
                },
            },
            'ozone': {
                'required': False,
                'type': 'float',
                'min': 0,
                'doc': {
                    'description': 'Columnar density of total atmospheric ozone (DU)',
                    'sample': '380.37',
                    'source_systems': 'darksky',
                },
            },
            'uv_index': {
                'required': False,
                'type': 'integer',
                'min': 0,
                'doc': {
                    'description': 'Columnar density of total atmospheric ozone (DU)',
                    'sample': '380.37',
                    'source_systems': 'darksky',
                },
            },
        },
    },
}

RESOURCE_METHODS = ['GET', 'POST']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']
IF_MATCH = False
